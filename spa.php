<?php
/**
 * Template Name:Spa
 *
 * @package WordPress
 * @subpackage octa
 * @since octa 1.0
 */


	// Sidebar
	$spa_widget_position = cs_get_option('tx_sidebar_position_spa');

	// Slider
	$spa_slider_position = cs_get_option('all_slider_layout');
	

	$slider_position_spa = $spa_slider_position['sap_slider_layout'];

	// slider_groups
	$spa_slider_groups = $spa_slider_position['spa_slider'];

	// sticky post count
	$spa_slider_sticky = $spa_slider_position['spa_sticky_slider'];
	
	// Slider title transform
	$title_transform = cs_get_option('slider_link_transform');

	$query_args = array(
        'order'                 => 'DESC',
		'orderby'               => 'orderby',
		'posts_per_page' => 1,
		'ignore_sticky_posts' => 1
    );

	


	get_header(); 
?>

	
	<?php if($slider_position_spa=='sticky') :?>
        <div id="octa-post-slider" class="carousel slide" data-ride="carousel">
		    <!-- Wrapper for slides -->
		    <div class="carousel-inner" role="listbox">
			    <?php 
					$args = array(
						'posts_per_page' =>$spa_slider_sticky ,
						'category_name'          => 'spa',
						'post__in'  => get_option( 'sticky_posts' ),
						'ignore_sticky_posts' => 1
					);
					$query = new WP_Query( $args );

				    $i=0;
				    
				    while($query->have_posts()) : 
				    	$query->the_post();
				    		$active = ($i == 0 ? 'active' : '');
			    ?>

			    <div class="item <?php echo $active;?>">
			    	
			    	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> 
			    	
				    <div class="carousel-caption">
				       <?php if(get_the_time()):?>
							<!-- Post date time  -->
							<span><a href="<?php the_permalink();?>"><?php the_time('F j, Y'); ?></a></span>
						<?php endif;?> 
					    <h1 class="slider-title <?php echo $title_transform; ?>"><a href="<?php the_permalink();?>"><?php echo wp_trim_words( get_the_title(),5, ''); ?></a></h1>
					    <p class="slider-content"><?php echo wp_trim_words( get_the_excerpt(),10, ''); // post title   ?></p>
					  <a class="btn btn-border" href="<?php the_permalink();?>">Read More</a>
				    </div>
			    </div>
			    <?php $i++; endwhile;  wp_reset_query(); ?>

		    </div>

            

		  <!-- Controls -->
		   <a class="left-control carousel-control" href="#octa-post-slider" role="button" data-slide="prev">
			   <span class="icon-left ion-ios-arrow-thin-left" aria-hidden="true"></span>
			   <span class="sr-only"><?php esc_html_e('Previous', 'octa');?></span>
		   </a>
		   <a class="right-control carousel-control" href="#octa-post-slider"role="button" data-slide="next">
			    <span class="icon-right ion-ios-arrow-thin-right" aria-hidden="true"></span>
			    <span class="sr-only"><?php esc_html_e('Next', 'octa');?></span>
		    </a>
		</div>
    <?php else: ?>

		<div id="octa-image-slider" class="carousel slide">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php for($i = 0; $i < count($spa_slider_groups); $i++):
					$active = $i == 0 ? 'active' : '';
				?>
				<li data-target="#octa-image-slider" data-slide-to="<?php echo $i;?>" class="<?php echo $active;?>"></li>
				<?php endfor; ?>
			</ol>
			

		   <!-- Wrapper for slides -->
		    <div class="carousel-inner" role="listbox">
				<?php 
					$i = 0;
				if ( !empty($spa_slider_groups) ): ?>

				<?php foreach ( $spa_slider_groups as $slider ):
				  $active = $i == 0 ? 'active' : ''; ?>
				    <div class="item <?php echo $active;?>">
				    <?php if($slider['spa_slider_image']): ?>
				        <img src="<?php echo wp_get_attachment_url($slider['spa_slider_image']); ?>" alt="Carousel Slider">
				    <?php endif; ?>
				        <div class="carousel-caption">

				            <?php if($slider['spa_slider_title']): ?>
					       		<h1 class="<?php echo $title_transform; ?>"><?php echo $slider['spa_slider_title'];?></h1>
					        <?php endif; ?>

					        <?php if($slider['spa_slider_desc']): ?>
					       		<p class="slider-content"><?php echo $slider['spa_slider_desc'];?></p>
					        <?php endif; ?>

					        <?php if($slider['spa_slider_btn_text']): ?>
					        	<a class="btn btn-border" target="_blank"href="<?php echo $slider['spa_slider_btn_link'];?>"><?php echo $slider['spa_slider_btn_text'];?></a>
					        <?php endif; ?>
				        </div>
				    </div>
				<?php $i++;
				   endforeach;?>

				<?php endif; ?>

		    </div>

			  <!-- Controls -->
			<a class="left-control carousel-control" href="#octa-image-slider" role="button" data-slide="prev">
			   <span class="icon-left ion-ios-arrow-thin-left" aria-hidden="true"></span>
			   <span class="sr-only"><?php esc_html_e('Previous', 'octa');?></span>
		   	</a>
		   	<a class="right-control carousel-control" href="#octa-image-slider"role="button" data-slide="next">
			    <span class="icon-right ion-ios-arrow-thin-right" aria-hidden="true"></span>
			    <span class="sr-only"><?php esc_html_e('Next', 'octa');?></span>
		    </a>
		</div>
	<?php endif; ?>

	<div class="container">
		<div class="row spa-category-item padding">

			<?php 
			// WP_Query arguments
			$spa_args = array (
				'category_name'          => 'spa',
				'category__not_in' => '3',
			);

			// The Query
			$spa_query = new WP_Query( $spa_args);

			if($spa_query->have_posts()){

				while($spa_query->have_posts()){

					$spa_query->the_post();

				}
			}

			wp_reset_query();



			   $term_id = 225;
			   $taxonomy_name = 'category';
			   $termchildren = get_term_children($term_id, $taxonomy_name);

			   ?>
			 
			    <?php foreach ($termchildren as $child): ?>
			        <?php $term = get_term_by('id', $child, $taxonomy_name);?>
			          <div class="col-md-4 col-md-12 col-xs-12">
				      	<div class="spa-image text-center">

				        <?php 
				           	
				           	$term_id = get_cat_ID( $term->name );
							$term_data = get_term_meta( $term_id, 'octa_cat_image', true );
							if($term_data):
								$term_data = $term_data['cat_image'];?>
							<a href="<?php echo get_term_link($child, $taxonomy_name); ?>">
								<div class="image-opacity">
									<?php echo wp_get_attachment_image($term_data, 'full');?>
								</div>
							</a>
								
						<?php endif;?>

							<div class="category-details">
								<header class="entry-header">
									<h4>
										<a href="<?php echo get_term_link($child, $taxonomy_name); ?>"><?php echo $term->name ?></a>
									</h4>
								</header>
								<div class="entry-content">
					           		<a class="btn btn-active" href="<?php echo get_term_link($child, $taxonomy_name); ?>"><?php esc_html_e('About Category', 'octa')?></a>
					           	</div> 
				           	</div>
				        </div>
			        </div>
			<?php endforeach; ?>
		</div>
	</div>

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">


			<div class="container">

				<div class="row">

		 			<!-- start left sidebar -->

			    	<?php if($spa_widget_position=='left') :?>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					<?php endif; ?>

					<!-- end left sidebar -->

		        	<!-- end of /.col-md-8 or /.col-md-12 -->

					<?php if($spa_widget_position=='no_sidebar') :?>
		                <div class="col-md-12">
		            <?php else: ?>
		                <div class="col-md-8">
		            <?php endif; ?>

			        		<div class="list">
			        		<?php 

			        		$posts_per_page = get_option( 'posts_per_page' );
							$paged = 1;
							if ( get_query_var( 'paged' ) ) $paged = get_query_var( 'paged' );
							if ( get_query_var( 'page' ) ) $paged = get_query_var( 'page' );


			        			$spa_post_args = array (
									'category_name'  => 'spa',
									'post_type' => 'post',
									'paged' => $paged,
									'posts_per_page' => $posts_per_page
								);

								// The Query
								$spa_query_post = new WP_Query( $spa_post_args); 
							?>

			            		<?php if($spa_query_post->have_posts()): ?>

					                <?php while($spa_query_post->have_posts()): ?>

										<?php $spa_query_post->the_post(); ?>

										<?php get_template_part( 'template-parts/content', 'spa' ); ?>
								
									<?php endwhile; ?>
							
		                		<?php endif; ?>



			                		<?php
									wp_reset_query();

			                	?>
			          
			        		</div>
			        		<!-- end of /.item -->

			        		<div class="octa-pagination text-center">
								<?php 
								$links = paginate_links( array(
										'prev_text'          => __('« Previous Page', 'octa'),
										'next_text'          => __('Next Page »', 'octa'),
										'total' => $spa_query_post->max_num_pages
									) );

									if ( $links ) {
										echo '<nav class="posts-pagination" role="navigation">';
											echo $links;
										echo '</nav>';
									}

									wp_reset_postdata();
								?>

	        		
        					</div>

			        	</div>
			        	<!-- end of /.col-md-8 or /.col-md-12 -->

		    	    <!-- start right sidebar -->

			    	<?php if($spa_widget_position=='right') :?>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					<?php endif; ?>

				    <!-- end right sidebar -->


				</div>
				<!-- end of /.row -->
			</div>
			<!-- end of /.container -->

		</main><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>

