jQuery(document).ready(function($){
  "use strict";

/* ========================================================================= */
/*  Search toggle
/* ========================================================================= */
   // header-search-form   
    $('.s-toggle').on('click', function(){
      $('.header-search-form').toggleClass('search-toggle');
      $(this).toggleClass("ion-search ion-close active");
    });


/* ========================================================================= */
/*  Smooth scroll
/* ========================================================================= */

    // Scroll top script
    $('.scroll-top').click(function(){
      $('body,html').animate({scrollTop:0},1000);
    });


/* ========================================================================= */
/*  Page loader
/* ========================================================================= */

  // Page loader  
   $(window).load(function() {
        $('.preloader').addClass('animated fadeOut');
        setTimeout(function() {
            $('.preloader').fadeOut(500)
        }, 500);
    });

/* ========================================================================= */
/*  Smooth scroll
/* ========================================================================= */

    // Smoothscroll 
    codeless_smoothScroll();

    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    function codeless_smoothScroll(){
        "use strict";
        try {
            $.browserSelector();
            if($("html").hasClass("chrome")) {
                $.smoothScroll();
            }
        } catch(err) {

        }
    }

/* ========================================================================= */
/*  Blog image owl carousel
/* ========================================================================= */

    jQuery(".blog-img").addClass("owl-carousel").owlCarousel({
       pagination: true,
       dots:false,
       loop:true,
       items:1,
       nav: true,
       navClass: ['owl-carousel-left','owl-carousel-right'],
       navText: ['<i class="ion-ios-arrow-thin-left""></i>', '<i class="ion-ios-arrow-thin-right"></i>'],
       margin:10,
       autoHeight : true,
       autoplay:false,
       responsive:{
           0:{
               items:1
           },
           600:{
               items:1
           },
           1000:{
               items:4
           }
       }
    });


/* ========================================================================= */
/*  Personal page image owl carousel
/* ========================================================================= */

    jQuery(".personal-slider").addClass("owl-carousel").owlCarousel({
       pagination: true,
       dots:true,
       loop:true,
       items:1,
       nav: true,
       navClass: ['owl-carousel-left','owl-carousel-right'],
       navText: ['<i class="ion-ios-arrow-thin-left""></i>', '<i class="ion-ios-arrow-thin-right"></i>'],
       margin:10,
       autoHeight : true,
       autoplay:false,
       responsive:{
           0:{
               items:1
           },
           600:{
               items:1
           },
           1000:{
               items:1
           }
       }
    });


/* ========================================================================= */
/*  Post masunary script
/* ========================================================================= */

  if($('.grid .grid-item').length){
      // vanilla JS
      // init with element
      var grid = document.querySelector('.grid');
      var msnry = new Masonry( grid, {
        // options...
        itemSelector: '.grid-item',
        columnWidth: 200
      });

      // init with selector
      var msnry = new Masonry( '.grid', {
        // options...
      });
  };

});


