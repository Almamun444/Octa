<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package octa
 */

	$widget_position = cs_get_option('tx_sidebar_position');

	get_header(); 

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="container">
			    <div class="row">

			 		<?php if ('left' == $widget_position): ?>
	                    <div class="col-md-4 col-sm-12">
	                        <?php get_sidebar(); ?>
	                    </div>                       
	                <?php endif; ?>

					<?php if ('no-sidebar' == $widget_position): ?>
	                    <div class="col-md-12 single-post">
	                <?php else: ?>
	                    <div class="col-md-8 single-post">
	                <?php endif; ?>

						<?php
						while ( have_posts() ) : the_post();

							get_template_part( 'template-parts/content', 'single');?>

							<div class="post-navigation">
			                    <div class="nav-previous">

			                    	<?php previous_post('%', 'Previous Post', 'no'); ?>
			                    </div>

								<div class="nav-next">
			                        <?php next_post('%', 'Next Post','no'); ?>
			                    </div>
		                    </div>
							
						<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;

							endwhile; // End of the loop.
						?>
					</div>
					<?php if ('right' == $widget_position): ?>
	                    <div class="col-md-4 col-sm-12">
	                        <?php get_sidebar(); ?>
	                    </div>
	                <?php endif; ?>
				</div>
			</div>
	    </main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
