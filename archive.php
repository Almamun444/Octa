<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

$widget_position = cs_get_option('tx_sidebar_position');

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		    <div class="container">
			    <div class="row">
			        <div class="col-md-12">
					    <?php
							if ( have_posts() ) : ?>

							<header class="page-header text-center">
								<?php
									the_archive_title( '<h1 class="page-title">', '</h1>' );
								?>
							</header><!-- .page-header -->
						<?php endif; ?>
					</div>
				</div>
			</div>


			<div class="container">
				<div class="row">

		    	<?php if($widget_position=='left') :?>
		 			<!-- start left sidebar -->
					<div class="col-md-4 col-sm-12 col-xs-12">
						<?php get_sidebar(); ?> 
					</div>
					<!-- end left sidebar -->
					<?php endif; ?>

					<?php if($widget_position=='no_sidebar') :?>
	                    <div class="col-md-12">
	                <?php else: ?>
	                    <div class="col-md-8">
	                <?php endif; ?>


					<?php
					if ( have_posts() ) : ?>

						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content', get_post_format() );

						endwhile;

							the_posts_navigation();
							octa_pagination();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>
				</div>
			    	<?php if($widget_position=='right') :?>
			    	<!-- start right sidebar -->
						<div class="col-md-4 col-sm-4 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					<!-- end right sidebar -->
					<?php endif; ?>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
