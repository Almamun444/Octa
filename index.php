<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

	// Sidebar
	$widget_position = cs_get_option('tx_sidebar_position');

	// Slider
	$default_slider_position = cs_get_option('all_slider_layout');
	

	$slider_position = $default_slider_position['slider_layout'];

	// slider_groups
	$slider_groups = $default_slider_position['home_slider'];

	// sticky post count
	$slider_sticky = $default_slider_position['sticky_slider'];
	
	// Slider title transform
	$title_transform = cs_get_option('slider_link_transform');


	$args = array(
		'posts_per_page' =>$slider_sticky ,
		'post__in'  => get_option( 'sticky_posts' ),
		'category_name'          => 'fashion',
		'ignore_sticky_posts' => 1
	);
	$query = new WP_Query( $args );




 	$query_args = array(
        'order'                 => 'DESC',
		'orderby'               => 'orderby',
		'posts_per_page' => 1,

		'ignore_sticky_posts' => 1
    );


get_header(); 
?>

	<?php if(is_front_page()): ?>

		<?php if($slider_position=='sticky') :?>
	        <div id="octa-post-slider" class="carousel slide" data-ride="carousel">
			    <!-- Wrapper for slides -->
			    <div class="carousel-inner" role="listbox">
				    <?php 
					    $i=0;
					    
					    while($query->have_posts()) : $query->the_post();
					    $active = ($i == 0 ? 'active' : '');
				    ?>

				    <div class="item <?php echo $active;?>">
				    	
				    	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> 
				    	
					    <div class="carousel-caption">
					       <?php if(get_the_time()):?>
								<!-- Post date time  -->
								<span><a href="<?php the_permalink();?>"><?php the_time('F j, Y'); ?></a></span>
							<?php endif;?> 
						    <h1 class="slider-title <?php echo $title_transform; ?>"><a href="<?php the_permalink();?>"><?php echo wp_trim_words( get_the_title(),5, ''); ?></a></h1>
						    <p class="slider-content"><?php echo wp_trim_words( get_the_excerpt(),10, ''); // post title   ?></p>
						  <a class="btn btn-border" href="<?php the_permalink();?>">Read More</a>
					    </div>
				    </div>
				    <?php $i++; endwhile;  wp_reset_query(); ?>

			    </div>

	            

			  <!-- Controls -->
			   <a class="left-control carousel-control" href="#octa-post-slider" role="button" data-slide="prev">
				   <span class="icon-left ion-ios-arrow-thin-left" aria-hidden="true"></span>
				   <span class="sr-only"><?php esc_html_e('Previous', 'octa');?></span>
			   </a>
			   <a class="right-control carousel-control" href="#octa-post-slider"role="button" data-slide="next">
				    <span class="icon-right ion-ios-arrow-thin-right" aria-hidden="true"></span>
				    <span class="sr-only"><?php esc_html_e('Next', 'octa');?></span>
			    </a>
			</div>
        <?php else: ?>

			<div id="octa-image-slider" class="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<?php for($i = 0; $i < count($slider_groups); $i++):
						$active = $i == 0 ? 'active' : '';
					?>
					<li data-target="#octa-image-slider" data-slide-to="<?php echo $i;?>" class="<?php echo $active;?>"></li>
					<?php endfor; ?>
				</ol>
				

			   <!-- Wrapper for slides -->
			    <div class="carousel-inner" role="listbox">
					<?php 
						$i = 0;
					if ( !empty($slider_groups) ): ?>

					<?php foreach ( $slider_groups as $slider ):
					  $active = $i == 0 ? 'active' : ''; ?>
					    <div class="item <?php echo $active;?>">
					    <?php if($slider['slider_image']): ?>
					        <img src="<?php echo wp_get_attachment_url($slider['slider_image']); ?>" alt="Carousel Slider">
					    <?php endif; ?>
					        <div class="carousel-caption">

					            <?php if($slider['slider_title']): ?>
						       		<h1 class="<?php echo $title_transform; ?>"><?php echo $slider['slider_title'];?></h1>
						        <?php endif; ?>

						        <?php if($slider['slider_desc']): ?>
						       		<p class="slider-content"><?php echo $slider['slider_desc'];?></p>
						        <?php endif; ?>

						        <?php if($slider['slider_btn_text']): ?>
						        	<a class="btn btn-border" target="_blank"href="<?php echo $slider['slider_btn_link'];?>"><?php echo $slider['slider_btn_text'];?></a>
						        <?php endif; ?>
					        </div>
					    </div>
					<?php $i++;
					   endforeach;?>

					<?php endif; ?>

			    </div>

				  <!-- Controls -->
				<a class="left-control carousel-control" href="#octa-image-slider" role="button" data-slide="prev">
				   <span class="icon-left ion-ios-arrow-thin-left" aria-hidden="true"></span>
				   <span class="sr-only"><?php esc_html_e('Previous', 'octa');?></span>
			   	</a>
			   	<a class="right-control carousel-control" href="#octa-image-slider"role="button" data-slide="next">
				    <span class="icon-right ion-ios-arrow-thin-right" aria-hidden="true"></span>
				    <span class="sr-only"><?php esc_html_e('Next', 'octa');?></span>
			    </a>
			</div>
								
        <?php endif; ?>

		

		<div class="container">
			<div class="row category-item padding">

				<?php 
					$sticky = array(
						'posts_per_page' =>3 ,
						'category_name'          => 'fashion',
						'post__in'  => get_option( 'sticky_posts' ),
						'ignore_sticky_posts' => 1
					);
					$sticky = new WP_Query( $sticky ); 
					
				while($sticky->have_posts()) : $sticky->the_post();
				?>
					<div class="col-md-4">
						<div class="post-img">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> 
							<div class="border-separate"></div>
							<div class="entry-meta">
								<?php the_category(); ?>
							</div>
						</div>
					</div>
				<?php endwhile;  wp_reset_query(); ?>
			</div>
		</div>
	<?php endif; ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<div class="row">

			    	<?php if($widget_position=='left') :?>
			 			<!-- start left sidebar -->
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
						<!-- end left sidebar -->
					<?php endif; ?>

					<?php if($widget_position=='no_sidebar') :?>
	                    <div class="col-md-12">
	                <?php else: ?>
	                    <div class="col-md-8">
	                <?php endif; ?>
					

					<div class="row">
	                	<div class="single-full-width">
	                	    <div class="col-md-12 col-sm-12 col-xs-12">
                				<?php  
									$query = new Wp_Query($query_args);
									while($query->have_posts()) : $query->the_post();?>
					
								<header class="entry-header text-center">
									<?php if(has_category()):?>
										<!-- Post category -->
										<span><?php the_category(); ?></span>
									<?php endif;?> 


									<?php
										the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );

									if ( 'post' === get_post_type() ) : ?>
				
									<div class="entry-meta">
									   <?php if(get_the_date()):?>
											<!-- Post date time  -->
											<span><a href="<?php the_permalink();?>"><?php the_date(); ?></a></span>
									    <?php endif;?>
										<span>
										    <a href="<?php the_permalink();?>">
											  	<?php comments_number( '- no responses', '- 1 Comments', '% responses' ); ?>
											</a>
										</span>
									</div><!-- .entry-meta -->
								<?php endif; ?>
							    </header><!-- .entry-header -->

								<div class="post-thumbnails"> 
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
								</div> 

								<div class="entry-content">
			
									<p class="text-center"><?php echo wp_trim_words( get_the_content(),30, ''); ?></p>	
							    </div>

								<footer class="entry-footer">
										<div class="content-btn text-center">
											<a href="<?php the_permalink(); ?>" class="btn btn-border"><?php esc_html_e('Continue Reading', 'octa');?></a>
										</div>
										<div class="author-deatils">
											<div class="author text-left">
												<a href="<?php the_permalink();?>">
													<i>
														<?php esc_html_e('By :', 'octa'); ?>
														<?php the_author(); ?>
													</i>
												</a>
												
											</div>
											 <div class="social-share text-right">
												<?php echo octa_social_share(); ?>
											</div>
										</div>
								</footer><!-- .entry-footer -->
				    		<?php endwhile;  wp_reset_query(); ?>
	                		</div>
	                	</div>
	                </div>


                	<div class="row">
                		<div class="grid">

                			<?php if(have_posts()): ?>

		                		<?php  if ( is_home() && ! is_front_page() ) : ?>

									<header>
										<h1 class="page-title screen-reader-text">
											<?php single_post_title(); ?>
										</h1>
									</header>

								<?php endif;  ?>

	                			<?php 

	                				while ( have_posts() ) : the_post();
	                				?>
	                				<div class="col-md-6 col-sm-12 col-xs-12 grid-item">
	                					<?php 

											get_template_part( 'template-parts/content', get_post_format() );

											?>

	                				</div>

									<?php

									endwhile;

									//the_posts_navigation();


	                			?>
	                		<?php else : ?>

								<?php get_template_part( 'template-parts/content', 'none' ); ?>

                			<?php endif; ?>

                		</div>
                	</div>
	                <?php octa_pagination(); ?>

				</div>

		    	<?php if($widget_position=='right') :?>
		    	    <!-- start right sidebar -->
					<div class="col-md-4 col-sm-12 col-xs-12">
						<?php get_sidebar(); ?> 
					</div>
				    <!-- end right sidebar -->
				<?php endif; ?>
			</div>
		</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
