<?php 


if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
/**
 *
 * CSFramework Taxonomy Config
 *
 * @since 1.0
 * @version 1.0
 *
 */
function octa_category_options($options) {
  $options      = array();


  $options[]   = array(
    'id'       => 'octa_cat_image',
    'taxonomy' => 'category',
    'fields'   => array(

      array(
        'id'    => 'cat_image',
        'type'  => 'image',
        'title' => esc_html__('Category Image', 'octa'),
      ),

    ),
  );

  return $options;
}

add_filter('cs_taxonomy_options', 'octa_category_options');

