

<?php
/**
 * 
 *
 * @package WordPress
 * @subpackage Crystal Codestar Stylesheet..
 * @since Crystal 1.0
 */

function tx_codestar_stylesheet() {
	$body_bg_color = cs_get_option('tx_body_p_color');
	$body_text_color = cs_get_option('tx_body_color');

	$header_top_bg_color = cs_get_option('tx_header_top_color');
	$header_top_color = cs_get_option('tx_header_color');
	$header_bg_color = cs_get_option('tx_header_p_color');
	$color = cs_get_option('tx_header_link_color');


	$breadcrumb_bg_color = cs_get_option('breadcrumb_bg_color');


	$preloader_img = cs_get_option('tx_preloader_img');

	$preloader_bg_color = cs_get_option('tx_preloader_color');

	$site_color = cs_get_option('site_title_color');
	
	$header_color_option = cs_get_option('tx_header_color');

	$footer_social_size = cs_get_option('copyright_font_size');
	$footer_link_color = cs_get_option('tx_footer_link_color');



	$typo_switch = cs_get_option('tx_custom_typo');

	$body_typo = cs_get_option('tx_body_font');

	$header_typo = cs_get_option('tx_header_font');

	$post_title_typo = cs_get_option('tx_post_title_font');

	$header_h1 = cs_get_option('tx_h1_font_size');
	$header_h2 = cs_get_option('tx_h2_font_size');
	$header_h3 = cs_get_option('tx_h3_font_size');
	$header_h4 = cs_get_option('tx_h4_font_size');
	$header_h5 = cs_get_option('tx_h5_font_size');
	$header_h6 = cs_get_option('tx_h6_font_size');

	$paragraph_p = cs_get_option('tx_p_font_size');
	$menu_link_font_size = cs_get_option('tx_menu_link_size');

	$link_menu = cs_get_option('tx_menu_link_font');






	  echo "<link href='https://fonts.googleapis.com/css?family=". $body_typo['family'] .":". $body_typo['variant'] ."' rel='stylesheet' type='text/css'>";

	  echo "<link href='https://fonts.googleapis.com/css?family=". $header_typo['family'] .":". $header_typo['variant'] ."' rel='stylesheet' type='text/css'>";

	  echo "<link href='https://fonts.googleapis.com/css?family=". $link_menu['family'] .":". $link_menu['variant'] ."' rel='stylesheet' type='text/css'>";

	   echo "<link href='https://fonts.googleapis.com/css?family=". $post_title_typo['family'] .":". $post_title_typo['variant'] ."' rel='stylesheet' type='text/css'>";


?>


<style >


	body {
		color: <?php echo $body_text_color;?>;

	    <?php if(empty($typo_switch)) :?>
	    	font-family: 'PT Serif', serif;
	    	font-weight:500;
	    	font-size: 16px;
	    <?php else:?>
			font-family: <?php echo $body_typo['family'];?>;
	        font-weight: <?php echo $body_typo['variant'];?>;
	        font-size: <?php echo $paragraph_p;?>px;
	    <?php endif;?>


	}

    <?php if(empty($typo_switch)) :?>
	 	h1, h2, h3, h4, h5, h6 {
	     	font-family: Cabin, sans-serif;
	     	font-weight: 700;
		}

		.navbar-nav li a{
	    	font-family: Cabin, sans-serif;
	    	font-weight: 300;
	    }
		.navbar-default .navbar-nav>li>a,
		ul.sub-menu li a{font-size:<?php echo $color['tx_header_link_size'];?>px; }

	<?php else:?>

	    h1, h2, h3, h4, h5, h6{
	    	font-family: <?php echo $header_typo['family'];?>;, sans-serif;
	    	font-weight: <?php echo $header_typo['variant'];?>;
	    }

	   .navbar-nav li a{
	    	font-family: <?php echo $link_menu['family'];?>;, sans-serif;
	    	font-weight: <?php echo $link_menu['variant'];?>;
	    }

	 	.entry-title a,.entry-title{
	 		font-family: <?php echo $post_title_typo['family'];?>;, sans-serif;
	    	font-weight: <?php echo $post_title_typo['variant'];?>;
	    	color:<?php echo $header_color_option['post_title_color'];?>;
	    	text-transform: <?php echo $footer_link_color['tx_footer_title_transform'];?>;
	    }

	    h1{ font-size: <?php echo $header_h1;?>px;}
	    h2{ font-size: <?php echo $header_h2;?>px;}
	    h3{ font-size: <?php echo $header_h3;?>px;}
	    h4{ font-size: <?php echo $header_h4;?>px;}
	    h5{ font-size: <?php echo $header_h5;?>px;}
	    h6{ font-size: <?php echo $header_h6;?>px;}
    	.navbar-default .navbar-nav>li>a,
		ul.sub-menu li a{font-size:<?php echo $menu_link_font_size;?>px; }


    <?php endif;?>


   .page-header{background: <?php echo $header_color_option['page_header_bg_color'];?>;
  		 text-transform: <?php echo $color['tx_header_link_transform'];?>;
  		 color:<?php echo $header_color_option['page_header_color'];?>;
  	}

  	.single-post-details{
  		background: <?php echo $header_color_option['page_header_bg_color'];?>;
  		padding: 5px 25px 15px 25px;
  	}

	.current-menu-item>a {
		color:<?php echo $color['tx_header_link_active'];?> !important;
		font-weight:700 !important;

	}

	.head-1 .s-toggle{color:<?php echo $color['tx_header_menu_1_reg'];?>;font-size:<?php echo $menu_link_font_size;?>px; right:-25px;}

	.head-2 .s-toggle{color:<?php echo $color['tx_header_menu_2_reg'];?>;font-size:<?php echo $menu_link_font_size;?>px; }



	.top-section{background-color:<?php echo $header_color_option['tx_header_top_bg_color'];?>;}
	.top-section a,
	.top-section li{color: <?php echo $header_color_option['tx_header_top_color'];?>;}

	.head-1 .navbar-default {
		background-color:<?php echo $header_color_option['tx_navbar_1_bg_color'];?>;
		border-color: <?php echo $header_color_option['tx_navbar_1_bg_color'];?>;
    }

    .head-2 .navbar-default {
		background-color:<?php echo $header_color_option['tx_navbar_2_bg_color'];?>;
		border-color: <?php echo $header_color_option['tx_navbar_2_bg_color'];?>;
    }


	.navbar-default .nav>li{
        list-style:none;
     
	}



	.head-1 .navbar-default .navbar-nav>li>a,
	ul.sub-menu li a{
		color:<?php echo $color['tx_header_menu_1_reg'];?>;
		text-transform: <?php echo $color['tx_header_link_transform'];?>;
 
    }

    .head-2 .navbar-default .navbar-nav>li>a,
	ul.sub-menu li a{
		color:<?php echo $color['tx_header_menu_2_reg'];?>;
		text-transform: <?php echo $color['tx_header_link_transform'];?>; 
    }



    .navbar-default .navbar-nav>li>a:focus, 
    .s-toggle:hover,
    .navbar-default .navbar-nav>li>a:hover{
		color:<?php echo $color['tx_header_link_hover'];?>;
    }


    .breadcrumb{
       background-color:<?php echo $breadcrumb_bg_color?>;
       color:#fff;
       border-radius: 0;
    }



   .preloader {
		position: fixed;
		left: 0px;
		top: 0px;
		width: 100%;
		height: 100%;
		z-index: 9999;
		background: url(<?php echo $preloader_img; ?>) center no-repeat <?php echo $preloader_bg_color; ?>;
	}


	.footer .widget-title{
		color: <?php echo $footer_link_color['footer_widget_title-color'];?>;
		text-transform: <?php echo $footer_link_color['tx_footer_title_transform'];?>;
	}

	.footer{
	    background-color:<?php echo $footer_link_color['tx_footer_p_color'];?>;
	    padding:50px 0;
	}


    .footer .footer-widget  a .post-heading{ color: <?php echo $footer_link_color['tx_footer_link_reg'];?>;}


    .copyright{
	    background-color:<?php echo $footer_link_color['tx_copyright_bg_color'];?>;;
	    border-top: 1px solid #616161;
	    padding: 20px 0 0;
	    color:<?php echo $footer_link_color['tx_footer_link_reg'];?>;
		font-size: <?php echo $footer_social_size;?>px;
	}

	.copyright .site-info a{
	    color: <?php echo $footer_link_color['tx_footer_link_color'];?>;	
	}

	.copyright .site-info a:hover{ color: <?php echo $footer_link_color['tx_footer_link_hover'];?>;}


</style>
<?php 
}
add_action( 'wp_head', 'tx_codestar_stylesheet' );