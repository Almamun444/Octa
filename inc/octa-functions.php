<?php
/**
 * octa functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package octa
 */


/**
 * Breadcumbs functions
 */
function octa_breadcrumb() {
	if (!is_home()) {
		echo '<a href="';
		echo home_url();
		echo '">';
		bloginfo('name');
		echo "</a> / ";
		if (is_category() || is_single()) {
			the_category('title_li=');
			if (is_single()) {
				echo " / ";
				the_title();
			}
		} elseif (is_page()) {
			echo the_title();
		}
	}
}




// Sanitaization

function octa_sanitize_text( $str ) {
	return sanitize_text_field( $str );
} 

function octa_sanitize_textarea( $text ) {
	return esc_textarea( $text );
} 

function octa_sanitize_number( $int ) {
	return absint( $int );
} 


// Octa sanitaization 
function octa_sanitize_file_url( $url ) {
	$output = '';
	$filetype = wp_check_filetype( $url );
	if ( $filetype["ext"] ) {
		$output = esc_url( $url );
	}
	return $output;
}



// Single social share
function octa_social_share( ) {


    remove_filter( 'the_title', 'wptexturize' );

    $tx_title = urlencode(html_entity_decode(get_the_title()));

    add_filter( 'the_title', 'wptexturize' );

    remove_filter( 'the_excerpt', 'wptexturize' );

    $tx_excerpt = urlencode(html_entity_decode(get_the_excerpt()));

    add_filter( 'the_excerpt', 'wptexturize' );

    $tx_url = urlencode( get_permalink() );



    // Construct sharing URL without using any script

    $twitter_url = 'https://twitter.com/intent/tweet?text='.$tx_title.'&amp;url='.$tx_url;
    $facebook_url = 'https://www.facebook.com/sharer/sharer.php?u='.$tx_url;
    $google_url = 'https://plus.google.com/share?url='.$tx_url;
    $linked_url = 'https://www.linkedin.com/shareArticle?mini=true&url='.$tx_url.'&title='.$tx_title.'&summary='.$tx_excerpt;


    ob_start();

    ?>


        <a class="facebook" href="<?php echo esc_url($facebook_url); ?>" target="_blank"><i class="ion-social-facebook"></i></a>

        <a class="twitter" href="<?php echo esc_url($twitter_url); ?>" target="_blank"><i class="ion-social-twitter"></i></a>

        <a  class="linkedin" href="<?php echo esc_url($linked_url); ?>" target="_blank"><i class="ion-social-linkedin-outline"></i></a>

        <a class="google" href="<?php echo esc_url($google_url); ?>" target="_blank"><i class="ion-social-googleplus"></i></a>


    <?php    $output = ob_get_clean();

    return $output;

}


/**
 * Pagination
 */
if ( ! function_exists( 'octa_pagination()' ) ) {
    function octa_pagination() {

        if( is_singular() )
            return;

        global $wp_query;

        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
            return;

        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $wp_query->max_num_pages );

        /**	Add current page to the array */
        if ( $paged >= 1 )
            $links[] = $paged;

        /**	Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
            $links[] = $paged - 1;
            $links[] = $paged - 2;
        }

        if ( ( $paged + 2 ) <= $max ) {
            $links[] = $paged + 2;
            $links[] = $paged + 1;
        }

        echo '<ul class="pagination">' . "\n";

        /**	Previous Post Link */
        if ( get_previous_posts_link() )
            printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

        /**	Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
            $class = 1 == $paged ? ' class="active"' : '';

            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

            if ( ! in_array( 2, $links ) )
                echo '<li><a href="#">…</a></li>';
        }

        /**	Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );
        foreach ( (array) $links as $link ) {
            $class = $paged == $link ? ' class="active"' : '';
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
        }

        /**	Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
            if ( ! in_array( $max - 1, $links ) )
                echo '<li><a href="#">…</a></li>' . "\n";

            $class = $paged == $max ? ' class="active"' : '';
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }

        /**	Next Post Link */
        if ( get_next_posts_link() )
            printf( '<li>%s</li>' . "\n", get_next_posts_link() );

        echo '</ul>' . "\n";

    }
}






