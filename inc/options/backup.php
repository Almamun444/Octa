<?php

function octa_backup_framework_options( $options ) {

// ------------------------------
	// Backup                       -
	// ------------------------------
	$options[]   = array(
		'name'     => 'backup_section',
		'title'    => esc_html__('Backup', 'octa'),
		'icon'     => 'fa fa-shield',
		'fields'   => array(

			array(
				'type'    => 'notice',
				'class'   => 'warning',
				'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'octa'),
			),

			array(
				'type'    => 'backup',
			),

		)
	);

	return $options;

}
add_filter( 'cs_framework_options', 'octa_backup_framework_options' );