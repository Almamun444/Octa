<?php

function octa_copyright_framework_options( $options ) {


    $options[]    = array(
        'name'      => 'tx_footer',
        'title'     => esc_html__('CopyRight Options', 'octa'),
        'icon'      => 'fa fa-copyright',
        'fields'    => array(
            /**
             * Copyright Text
             */

            array(
                'id'      => 'copyright_font_size',
                'type'    => 'number',
                'title'   =>  esc_html__('Copyright Font Size', 'octa'),
                'desc'    =>  esc_html__('Copyright font size.', 'octa'),
                'default' => '14',
            ),

            array(
                'id'    => 'copyright_text',
                'type'  => 'textarea',
                'title' => esc_html__('Copyright Text', 'octa'),
                'desc'  => esc_html__('Write copyright text here.', 'octa'),
            ),


        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'octa_copyright_framework_options' );