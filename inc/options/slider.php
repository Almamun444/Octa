<?php

function octa_slider_framework_options( $options ) {

    $options[]    = array(
        'name'      => 'tx_slider',
        'title'     => esc_html__('Slider Settings', 'octa'),
        'icon'      => 'fa fa-sliders',
        'fields'    => array(

          array(
                'id'        => 'slider_link_transform',
                'type'      => 'select',
                'title'     => esc_html__('Slider Title Transform', 'octa'),
                'desc'      => esc_html__('Select title transform.', 'octa'),
                'options'   => array(
                    'text-uppercase'     => esc_html__('Text Uppercase', 'octa'),
                    'text-lowercase'     => esc_html__('Text Lowercase', 'octa'),
                    'text-capitalize'    => esc_html__('Text Capitalize', 'octa'),

                ),
                'default'      => 'text-uppercase'
            ),

            array(
              'id'           => 'all_slider_layout',
              'type'         => 'fieldset',
              'title'        => esc_html__('Select Page Slider & Banner', 'octa'),
              'desc'         => esc_html__('Select slider for home page.', 'octa'),
              'fields'          => array(

                array(
                  'id'           => 'slider_layout',
                  'type'         => 'select',
                  'title'        => esc_html__('Default Home Page Slider', 'octa'),
                  'desc'         => esc_html__('Select slider for home page.', 'octa'),
                  'options' => array(
                      'sticky'   => esc_html__('Default Sticky Post Slider', 'octa'),
                      'image'    => esc_html__('Default Image Slider', 'octa'),
                  ),
                  'default' => 'sticky',
                ),


                  array(
                      'id'         => 'sticky_slider',
                      'type'       => 'text',
                      'title'      => esc_html__('Default Sticky Post Number', 'octa'),
                      'desc'       => esc_html__('Write sticky post number.', 'octa'),
                      'default'    => 3,
                      'dependency' => array('slider_layout', 'any', 'sticky'),
                  ),

                  array(
                      'id'              => 'home_slider',
                      'type'            => 'group',
                      'title'           => esc_html__('Slider Description', 'octa'),
                      'desc'            => esc_html__('Write slider desc.', 'octa'),
                      'button_title'    => esc_html__('Add New Slider', 'octa'),
                      'accordion_title' => esc_html__('Slider Itmes', 'octa'),
                      'dependency'      => array('slider_layout', 'any', 'image'),
                      'fields'          => array(

                        //Slider Options
                        array(
                          'id'          => 'slider_title',
                          'type'        => 'text',
                          'title'       => esc_html__('Write Slider Title' ,'octa'),
                          'default'     => ' A Comprehensive Guideline'
                        ),
                        array(
                          'id'          => 'slider_desc',
                          'type'        => 'textarea',
                          'title'       => esc_html__('Write Slider Description', 'octa'),
                          'default'     => 'Besides the popularity, more and more individuals and businesses of all sizes start'
                        ),
                        array(
                          'id'          => 'slider_image',
                          'type'        => 'image',
                          'title'       => esc_html__('Add Slider Image', 'octa'),
                          'default'     => 'http://octa.dev/wp-content/uploads/2016/09/622-1024x683@2x-150x150.jpg'
                        ),

                        array(
                          'id'          => 'slider_btn_text',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Slider Button Text', 'octa'),
                          'default'     => 'Read More'
                        ),

                        array(
                          'id'          => 'slider_btn_link',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Slider Button Link', 'octa'),
                          'default'     => '#'
                        ),

                      )
                    ),




               array(
                  'id'           => 'sap_slider_layout',
                  'type'         => 'select',
                  'title'        => esc_html__('Spa Home Page Slider', 'octa'),
                  'desc'         => esc_html__('Select slider for spa page.', 'octa'),
                  'options' => array(
                      'sticky'   => esc_html__('Spa Sticky Post Slider', 'octa'),
                      'image'    => esc_html__('Spa Image Slider', 'octa'),
                  ),
                  'default' => 'image',
                ),


                  array(
                      'id'         => 'spa_sticky_slider',
                      'type'       => 'text',
                      'title'      => esc_html__('Spa Sticky Post Number', 'octa'),
                      'desc'       => esc_html__('Write sticky post number.', 'octa'),
                      'default'    => 3,
                      'dependency' => array('sap_slider_layout', 'any', 'sticky'),
                  ),

                  array(
                      'id'              => 'spa_slider',
                      'type'            => 'group',
                      'title'           => esc_html__('Spa Slider Description', 'octa'),
                      'desc'            => esc_html__('Write slider desc.', 'octa'),
                      'button_title'    => esc_html__('Add New Slider', 'octa'),
                      'accordion_title' => esc_html__('Slider Itmes', 'octa'),
                      'dependency'      => array('sap_slider_layout', 'any', 'image'),
                      'fields'          => array(

                        //Slider Options
                        array(
                          'id'          => 'spa_slider_title',
                          'type'        => 'text',
                          'title'       => esc_html__('Write Slider Title' ,'octa'),
                          'default'     => 'Create Fashion Blog Wordpress Theme'
                        ),
                        array(
                          'id'          => 'spa_slider_desc',
                          'type'        => 'textarea',
                          'title'       => esc_html__('Write Slider Description', 'octa'),
                          'default'     => 'One Page Website is one of the hottest and newest trend in web Design. The fascinating trend are getting popular for number of factors.'
                        ),
                        array(
                          'id'          => 'spa_slider_image',
                          'type'        => 'image',
                          'title'       => esc_html__('Add Slider Image', 'octa'),
                          'default'     => 'http://octa.dev/wp-content/uploads/2016/10/slider-5.jpg'
                        ),

                        array(
                          'id'          => 'spa_slider_btn_text',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Slider Button Text', 'octa'),
                          'default'     => 'Read More'
                        ),

                        array(
                          'id'          => 'spa_slider_btn_link',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Slider Button Link', 'octa'),
                          'default'     => '#'
                        ),

                      )
                    ),



                array(
                  'id'           => 'personal_slider_layout',
                  'type'         => 'select',
                  'title'        => esc_html__('Personal Home Page Slider', 'octa'),
                  'desc'         => esc_html__('Select slider for personal page.', 'octa'),
                  'options' => array(
                      'image'    => esc_html__('Personal Image Slider', 'octa'),
                      'none'     => esc_html__('No Slider', 'octa'),
                  ),
                  'default' => 'image',
                ),

                  array(
                      'id'              => 'personal_slider',
                      'type'            => 'group',
                      'title'           => esc_html__('Personal Slider Description', 'octa'),
                      'desc'            => esc_html__('Write slider desc.', 'octa'),
                      'button_title'    => esc_html__('Add New Slider', 'octa'),
                      'accordion_title' => esc_html__('Slider Itmes', 'octa'),
                      'dependency'      => array('personal_slider_layout', 'any', 'image'),
                      'fields'          => array(

                        //Slider Options
                        array(
                          'id'          => 'personal_slider_title',
                          'type'        => 'text',
                          'title'       => esc_html__('Write Slider Title' ,'octa'),
                          'default'     => 'Welcome to Wordpress Theme'
                        ),
                        array(
                          'id'          => 'personal_slider_desc',
                          'type'        => 'textarea',
                          'title'       => esc_html__('Write Slider Description', 'octa'),
                          'default'     => 'One Page Website is one of the hottest and newest trend in web Design. The fascinating trend are getting popular for number of factors.'
                        ),
                        array(
                          'id'          => 'personal_slider_image',
                          'type'        => 'image',
                          'title'       => esc_html__('Add Slider Image', 'octa'),
                          'default'     => 'http://octa.dev/wp-content/uploads/2016/10/slider-5.jpg'
                        ),

                        array(
                          'id'          => 'personal_slider_btn_text',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Slider Button Text', 'octa'),
                          'default'     => 'Read More'
                        ),

                        array(
                          'id'          => 'personal_slider_btn_link',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Slider Button Link', 'octa'),
                          'default'     => '#'
                        ),

                      )
                    ),

              array(
                  'id'           => 'photography_banner_layout',
                  'type'         => 'select',
                  'title'        => esc_html__('Photography Home Page Banner', 'octa'),
                  'desc'         => esc_html__('Select banner for photography page.', 'octa'),
                  'options'      => array(
                      'banner'    => esc_html__('Photography Page Banner', 'octa'),
                      'video'     => esc_html__('Video Banner', 'octa'),
                  ),
                  'default' => 'banner',
                ),

                 array(
                    'id'          => 'photo_video_banner',
                    'type'        => 'upload',
                    'title'       => esc_html__('Upload Your Video', 'octa'),
                    'desc'        => esc_html__('upload video background.', 'octa'),
                    'dependency'  => array('photography_banner_layout', 'any', 'video'),
                    'default'     => 'http://octa.dev/wp-content/uploads/2016/10/SLIDESHOW-VIDEO-DISPLAY-AE-Template-With-Retro-Styled-Photo-Frames.mp4',
                      'settings'        => array(
                         'upload_type'  => 'video/mp4',
                         'button_title' => 'Upload Video',
                         'frame_title'  => 'Select a video',
                         'insert_title' => 'Use this video',
                        ),
                  ),


                  array(
                      'id'              => 'photo_banner',
                      'type'            => 'fieldset',
                      'title'           => esc_html__('Photography Banner Description', 'octa'),
                      'desc'            => esc_html__('Write banner desc.', 'octa'),
                      'dependency'      => array('photography_banner_layout', 'any', 'banner'),
                      'fields'          => array(

                        //Slider Options
                        array(
                          'id'          => 'photo_banner_title',
                          'type'        => 'text',
                          'title'       => esc_html__('Write Banner Title' ,'octa'),
                          'default'     => 'Photography Banner'
                        ),
                        array(
                          'id'          => 'photo_banner_desc',
                          'type'        => 'textarea',
                          'title'       => esc_html__('Write Banner Description', 'octa'),
                          'default'     => 'The fascinating trend are getting popular.'
                        ),
                   


                          array(
                              'id'           => 'photography_bg_layout',
                              'type'         => 'select',
                              'title'        => esc_html__('Banner Background Options', 'octa'),
                              'desc'         => esc_html__('Select banner or color.', 'octa'),
                              'options' => array(
                                  'image'    => esc_html__('Background Image', 'octa'),
                                  'color'    => esc_html__('Background Color', 'octa'),
                              ),
                              'default' => 'image',
                            ),

                                array(
                                  'id'          => 'photo_banner_bg_image',
                                  'type'        => 'image',
                                  'title'       => esc_html__('Add Banner Image', 'octa'),
                                  'desc'        => esc_html__('Add background image.', 'octa'),
                                  'dependency'  => array('photography_bg_layout', 'any', 'image'),
                                  'default'     => 'http://octa.dev/wp-content/uploads/2016/10/banner-3-150x150.jpg'
                                ),

                                  array(
                                      'id'           => 'tx_banner_parallax',
                                      'type'         => 'switcher',
                                      'title'        => esc_html__('Banner Background Parallax', 'octa'),
                                      'desc'         => esc_html__('Enable bg parallax.', 'octa'),
                                      'default'      => true,
                                      'dependency'      => array('photography_bg_layout', 'any', 'image'),
                                  ),



                                array(
                                  'id'          => 'photo_banner_bg_color',
                                  'type'        => 'color_picker',
                                  'title'       => esc_html__('Add Backgroundc Color', 'octa'),
                                  'desc'        => esc_html__('Add background color.', 'octa'),
                                   'dependency' => array('photography_bg_layout', 'any', 'color'),
                                  'default'     => '#000'
                                ),



                        array(
                          'id'          => 'photo_banner_btn_text',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Banner Button Text', 'octa'),
                          'default'     => 'Explore It'
                        ),

                        array(
                          'id'          => 'photo_banner_btn_link',
                          'type'        => 'text',
                          'title'       => esc_html__('Add Banner Button Link', 'octa'),
                          'default'     => '#'
                        ),

                      )
                    ),




                  )
                ),
              )
            );

    return $options;

}
add_filter( 'cs_framework_options', 'octa_slider_framework_options' );