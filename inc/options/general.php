<?php

function octa_general_framework_options( $options ) {

    $options      = array(); // remove old options

    $options[]    = array(
        'name'      => 'tx_general',
        'title'     => esc_html__('General Settings', 'octa'),
        'icon'      => 'fa fa-wrench',
        'fields'    => array(
             array(
              'id'          => '404_image',
              'type'        => 'image',
              'title'       => esc_html__('Add 404 Image', 'octa'),
              'desc'        => esc_html__('Add your 404 image.', 'octa'),
            ),

            /**
             * Preloader options
             */
            array(
                'id'            => 'tx-enable-preloader',
                'type'          => 'switcher',
                'title'         => esc_html__('Preloader Switcher', 'octa'),
                'desc'          => esc_html__('Enable or disable preloader', 'octa'),
                'default'       => true
            ),

            array(
                'id'      => 'tx_preloader_color',
                'type'    => 'color_picker',
                'title'   => esc_html__('Preloader Background Color', 'octa'),
                'desc'    => esc_html__('Add your background color.', 'octa'),
                'default' => '#fff',
                'dependency'        => array('tx-enable-preloader', '==', 'true'),

            ),

            array(
                'id'            => 'tx_preloader_img',
                'type'          => 'upload',
                'title'         => esc_html__('Preloader Gif Image', 'octa'),
                'desc'          => esc_html__('Add a gif image.', 'octa'),
                'settings'      => array(
                    'upload_type'  => 'image',
                    'button_title' => 'Upload',
                    'frame_title'  => esc_html__('Select an image', 'octa'),
                    'insert_title' => esc_html__('Use this image', 'octa'),
                ),
                'dependency'        => array('tx-enable-preloader', '==', 'true'),
            ),

            /**
             * Enable Breadcrumb
             */

            array(
                'id'           => 'tx_breadcrumb',
                'type'         => 'switcher',
                'title'        => esc_html__('Enable Breadcrumb?', 'octa'),
                'desc'         => esc_html__('Enable & Disable.', 'octa'),
                'default'      => false
            ),

            array(
                'id'        => 'breadcrumb_bg_color',
                'type'      => 'color_picker',
                'title'     => esc_html__('Breadcrumb background Color', 'octa'),
                'desc'      => esc_html__('Breadcrumb background color', 'octa'),
                
                'dependency'        => array('tx_breadcrumb', '==', 'true'),
            ),
        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'octa_general_framework_options' );