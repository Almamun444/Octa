<?php

/**
 * header setting for octa
 *
 * @return array [alias => title]
 */




function octa_header_framework_options( $options ) {



    $options[]    = array(
        'name'      => 'tx_header',
        'title'     => esc_html__('Header Settings', 'octa'),
        'icon'      => 'fa fa-header',
        'fields'    => array(

            /**
             *Header Column Select
             */

            array(
                'id'        => 'tx_header_select',
                'type'      => 'image_select',
                'title'     => esc_html__('Header Select', 'octa'),
                'desc'      => esc_html__('Select header variation.', 'octa'),
                'options'   => array(
                    'header_1' => get_template_directory_uri() . '/inc/options/images/header-1.jpg',
                    'header_2' => get_template_directory_uri() . '/inc/options/images/header-2.jpg',
                ),
                'default'   => 'header_1'

            ),


            array(
                'id'    => 'tx_logo_1',
                'type'  => 'image',
                'title' => esc_html__('Header 1 Site Logo', 'octa'),
                'desc'  => esc_html__('Upload a site logo for your header 1.', 'octa'),
            ),

            array(
                'id'    => 'tx_logo_2',
                'type'  => 'image',
                'title' => esc_html__('Header 2 Site Logo', 'octa'),
                'desc'  => esc_html__('Upload a site logo for your header 2.', 'octa'),
            ),


            /**
             * Social Icon Link
             */

            array(
                'id'           => 'tx_social_icons',
                'type'         => 'switcher',
                'title'        => esc_html__('Header Top Section', 'octa'),
                'desc'         => esc_html__('Enable/Disable header top section .', 'octa'),
                'default'      => true
            ),

                array(
                    'id'        => 'tx_top_text',
                    'type'      => 'fieldset',
                    'title'     => esc_html__('Top Contact Details', 'octa'),
                    'desc'      => esc_html__('Contact details top section.', 'octa'),
                    'fields'    => array(

                    array(
                        'id'    => 'tx_top_location',
                        'type'  => 'text',
                        'title' => esc_html__('Location', 'octa'),
                        'desc'  => esc_html__('Enter your location.', 'octa'),
                        'default'   => '12 royal street, california usa'
                    ),

                    array(
                        'id'    => 'tx_top_call',
                        'type'  => 'text',
                        'title' => esc_html__('Contact', 'octa'),
                        'desc'  => esc_html__('Enter your contact.', 'octa'),
                        'default'   => '011-122-134-55'
                    ),

                    array(
                        'id'    => 'tx_top_mail',
                        'type'  => 'text',
                        'title' => esc_html__('Email', 'octa'),
                        'desc'  => esc_html__('Enter your email.', 'octa'),
                        'default'   => 'contact@domain.com'
                    ),
                ),
                'dependency'   => array( 'tx_social_icons', '==', 'true' ),
            ),

            array(
                'id'        => 'tx_top_social',
                'type'      => 'fieldset',
                'title'     => esc_html__('Top Social Link', 'octa'),
                'desc'      => esc_html__('Social link top section.', 'octa'),
                'fields'    => array(

                    array(
                        'id'    => 'tx_top_social_fb',
                        'type'  => 'text',
                        'title' => esc_html__('Facebook', 'octa'),
                        'desc'  => esc_html__('Enter your facebook link.', 'octa'),
                    ),

                    array(
                        'id'    => 'tx_top_social_tw',
                        'type'  => 'text',
                        'title' => esc_html__('Twitter', 'octa'),
                        'desc'  => esc_html__('Enter your twitter link.', 'octa')
                    ),
                    array(
                        'id'    => 'tx_top_social_gm',
                        'type'  => 'text',
                        'title' => esc_html__('Google plus', 'octa'),
                        'desc'  => esc_html__('Enter your google plus link.', 'octa')
                    ),
                    array(
                        'id'    => 'tx_top_social_vm',
                        'type'  => 'text',
                        'title' => esc_html__('Linkedin', 'octa'),
                        'desc'  => esc_html__('Enter your linkedin link.', 'octa')
                    ),
                    array(
                        'id'    => 'tx_top_social_pi',
                        'type'  => 'text',
                        'title' => esc_html__('Pinterest', 'octa'),
                        'desc'  => esc_html__('Enter your pinterest link.', 'octa')
                    ),


                ),
                'dependency'   => array( 'tx_social_icons', '==', 'true' ),
            ),
            // ------------------------------------

        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'octa_header_framework_options' );