<?php

function octa_sidebar_framework_options( $options ) {


    $options[]    = array(
        'name'      => 'tx_sidebar',
        'title'     => esc_html__('Sidebar', 'octa'),
        'icon'      => 'fa fa-th-list',
        'fields'    => array(

            /**
             * Sidebar Position Default/fashion
             */

            array(
                'id'        => 'tx_sidebar_position',
                'type'      => 'select',
                'title'     => esc_html__('Home Page Sidebar Position', 'octa'),
                'options'   => array(
                    'left'   => 'Left',
                    'right'   => 'Right',
                    'no_sidebar'   => 'No Sidebar',
                ),
                'default'   => 'right',
                'desc'      => esc_html__('Select home page sidebar position.', 'octa'),
            ),

            /**
             * Sidebar Position spa
             */

            array(
                'id'        => 'tx_sidebar_position_spa',
                'type'      => 'select',
                'title'     => esc_html__('Spa Page Sidebar Position', 'octa'),
                'options'   => array(
                    'left'   => 'Left',
                    'right'   => 'Right',
                    'no_sidebar'   => 'No Sidebar',
                ),
                'default'   => 'right',
                'desc'      => esc_html__('Select spa page sidebar position.', 'octa'),
            ),

            /**
             * Sidebar Position photography
             */

            array(
                'id'        => 'tx_sidebar_position_personal',
                'type'      => 'select',
                'title'     => esc_html__('Personal Page Sidebar Position', 'octa'),
                'options'   => array(
                    'left'   => 'Left',
                    'right'   => 'Right',
                    'no_sidebar'   => 'No Sidebar',
                ),
                'default'   => 'right',
                'desc'      => esc_html__('Select personal page sidebar position.', 'octa'),
            ),

            /**
             * Sidebar Position Agency
             */

            array(
                'id'        => 'tx_sidebar_position_photo',
                'type'      => 'select',
                'title'     => esc_html__('Photography Page Sidebar Position', 'octa'),
                'options'   => array(
                    'left'   => 'Left',
                    'right'   => 'Right',
                    'no_sidebar'   => 'No Sidebar',
                ),
                'default'   => 'no_sidebar',
                'desc'      => esc_html__('Select photography sidebar position.', 'octa'),
            ),

            /**
             * Sidebar Position Corporate
             */

            // array(
            //     'id'        => 'tx_sidebar_position_corporate',
            //     'type'      => 'select',
            //     'title'     => esc_html__('Corporate Sidebar Position', 'octa'),
            //     'options'   => array(
            //         'left'   => 'Left',
            //         'right'   => 'Right',
            //         'no_sidebar'   => 'No Sidebar',
            //     ),
            //     'default'   => 'right',
            //     'desc'      => esc_html__('Select default sidebar position.', 'octa'),
            // ),

            /**
             * Sidebar Position Travel
             */

            // array(
            //     'id'        => 'tx_sidebar_position_travel',
            //     'type'      => 'select',
            //     'title'     => esc_html__('Travel Sidebar Position', 'octa'),
            //     'options'   => array(
            //         'left'   => 'Left',
            //         'right'   => 'Right',
            //         'no_sidebar'   => 'No Sidebar',
            //     ),
            //     'default'   => 'right',
            //     'desc'      => esc_html__('Select default sidebar position.', 'octa'),
            // ),

            /**
             * Sidebar Position Megazine
             */

            // array(
            //     'id'        => 'tx_sidebar_position_megazine',
            //     'type'      => 'select',
            //     'title'     => esc_html__('Megazine Sidebar Position', 'octa'),
            //     'options'   => array(
            //         'left'   => 'Left',
            //         'right'   => 'Right',
            //         'no_sidebar'   => 'No Sidebar',
            //     ),
            //     'default'   => 'right',
            //     'desc'      => esc_html__('Select default sidebar position.', 'octa'),
            // ),

            /**
             * Sidebar Position Photography
             */

            // array(
            //     'id'        => 'tx_sidebar_position_photography',
            //     'type'      => 'select',
            //     'title'     => esc_html__('Photography Sidebar Position', 'octa'),
            //     'options'   => array(
            //         'left'   => 'Left',
            //         'right'   => 'Right',
            //         'no_sidebar'   => 'No Sidebar',
            //     ),
            //     'default'   => 'right',
            //     'desc'      => esc_html__('Select default sidebar position.', 'octa'),
            // ),



        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'octa_sidebar_framework_options' );