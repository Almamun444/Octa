<?php

function typo_framework_options( $options ) {


    $options[]    = array(
        'name'      => 'tx_typo',
        'title'     => esc_html__('Typography', 'octa'),
        'icon'      => 'fa fa-text-height',
        'fields'    => array(

            /**
             * Custom Typography
             */

            array(
                'id'           => 'tx_custom_typo',
                'type'         => 'switcher',
                'title'        => esc_html__('Custom Typography', 'octa'),
                'desc'         => esc_html__('Enable custom typography.', 'octa'),
                'default'      => false
            ),

            /**
             * Body Font Select
             */

            array(
                'id'        => 'tx_body_font',
                'type'      => 'typography',
                'title'     => esc_html__('Select Body Font', 'octa'),
                'desc'      => esc_html__('Body font family.', 'octa'),
                'default'   => array(
                    'family'  => 'PT Serif',
                    'font'    => 'google', // this is helper for output ( google, websafe, custom )
                    'variant' => '500',
                ),
                'dependency'  => array( 'tx_custom_typo', '==', 'true' ),
            ),


            /**
             * Body Font Size
            */

            array(
                'id'      => 'tx_p_font_size',
                'type'    => 'number',
                'title'   =>  esc_html__('Body Font Size', 'octa'),
                'desc'    =>  esc_html__('Body font size.', 'octa'),
                'default' => '12',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            /**
             * Link Font Select
             */

            array(
                'id'        => 'tx_menu_link_font',
                'type'      => 'typography',
                'title'     => esc_html__('Select Menu Link Font', 'octa'),
                'desc'      => esc_html__('Menu link font family.', 'octa'),
                'default'   => array(
                    'family'  => 'Cabin',
                    'font'    => 'google', // this is helper for output ( google, websafe, custom )
                    'variant' => '500',
                ),
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_menu_link_size',
                'type'    => 'number',
                'title'   =>  esc_html__('Menu Link Font Size', 'octa'),
                'desc'    =>  esc_html__('Menu font size.', 'octa'),
                'default' => '14',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            array(
                'id'        => 'tx_post_title_font',
                'type'      => 'typography',
                'title'     => esc_html__('Select Post Title Font', 'octa'),
                'desc'      => esc_html__('Post title (h2) font family.', 'octa'),
                'default'   => array(
                    'family'  => 'Cabin',
                    'font'    => 'google', // this is helper for output ( google, websafe, custom )
                    'variant' => '500',
                ),
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),


            /**
             * Header Font Select
             */

            array(
                'id'        => 'tx_header_font',
                'type'      => 'typography',
                'title'     => esc_html__('Select Heading Font', 'octa'),
                'desc'      => esc_html__('Header font family.', 'octa'),
                'default'   => array(
                    'family'  => 'Cabin',
                    'font'    => 'google', // this is helper for output ( google, websafe, custom )
                    'variant' => '700',
                ),
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),


            /**
             * Header 1 Font Size
             */

            array(
                'id'      => 'tx_h1_font_size',
                'type'    => 'number',
                'title'   => esc_html__( 'H1 - Font Size', 'octa'),
                'desc'    => esc_html__('Header 1 (h1) font size.', 'octa'),
                'default' => '32',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            /**
             * Header 2 Font Size
             */

            array(
                'id'      => 'tx_h2_font_size',
                'type'    => 'number',
                'title'   =>  esc_html__('H2 - Font Size', 'octa'),
                'desc'    =>  esc_html__('Header 2 (h2) font size.', 'octa'),
                'default' => '28',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            /**
             * Header 3 Font Size
             */

            array(
                'id'      => 'tx_h3_font_size',
                'type'    => 'number',
                'title'   =>  esc_html__('H3 - Font Size', 'octa'),
                'desc'    =>  esc_html__('Header 3 (h3) font size.', 'octa'),
                'default' => '24',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            /**
             * Header 4 Font Size
             */

            array(
                'id'      => 'tx_h4_font_size',
                'type'    => 'number',
                'title'   =>  esc_html__('H4 - Font Size', 'octa'),
                'desc'    =>  esc_html__('Header 4 (h4) font size.', 'octa'),
                'default' => '20',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            /**
             * Header 5 Font Size
             */

            array(
                'id'      => 'tx_h5_font_size',
                'type'    => 'number',
                'title'   =>  esc_html__('H5 - Font Size', 'octa'),
                'desc'    =>  esc_html__('Header 5 (h5) font size.', 'octa'),
                'default' => '16',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

            /**
             * Header 6 Font Size
             */

            array(
                'id'      => 'tx_h6_font_size',
                'type'    => 'number',
                'title'   =>  esc_html__('H6 - Font Size', 'octa'),
                'desc'    =>  esc_html__('Header 6 (h6) font size.', 'octa'),
                'default' => '14',
                'dependency'   => array( 'tx_custom_typo', '==', 'true' ),
            ),

        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'typo_framework_options' );