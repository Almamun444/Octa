<?php

function style_framework_options( $options ) {

    $options[]    = array(
        'name'      => 'tx_style',
        'title'     => 'Theme Style',
        'icon'      => 'fa fa-paint-brush',
        'fields'    => array(

                        /**
             * Body Secondary Color
             */

            array(
                'id'      => 'tx_body_color',
                'type'    => 'color_picker',
                'title'   => esc_html__('Body Text Color', 'octa'),
                'desc'    => esc_html__('Select body text color.', 'octa'),
                'default' => '#404040',
            ),


            /**
             * Header Link Color
             */

            array(
                'id'        => 'tx_header_link_color',
                'type'      => 'fieldset',
                'title'     => esc_html__('Menu Link Options', 'octa'),
                'desc'      => esc_html__('Select menu link color.', 'octa'),
                'fields'    => array(

                    array(
                        'id'    => 'tx_header_link_transform',
                        'type'  => 'select',
                        'title' => esc_html__('Menu Transform', 'octa'),
                        'desc'  => esc_html__('Select your menu transform.', 'octa'),
                        'options'   => array(
                            'uppercase'     => esc_html__('Text Uppercase', 'octa'),
                            'lowercase'     => esc_html__('Text Lowercase', 'octa'),
                            'capitalize'    => esc_html__('Text Capitalize', 'octa'),

                        ),
                        'default' => 'uppercase'
                    ),

                    array(
                        'id'      => 'tx_header_link_size',
                        'type'    => 'text',
                        'title'   => esc_html__('Default Menu Link Size', 'octa'),
                        'desc'    => esc_html__('Write your default menu text size.', 'octa'),
                        'default' => '12',
                    ),

                    array(
                        'id'      => 'tx_header_menu_1_reg',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Header 1 Regular Color', 'octa'),
                        'desc'    => esc_html__('Add your header 1 menu text color.', 'octa'),
                        'default' => '#111',
                    ),

                    array(
                        'id'      => 'tx_header_menu_2_reg',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Header 2 Regular Color', 'octa'),
                        'desc'    => esc_html__('Add your header 2 menu text color.', 'octa'),
                        'default' => '#fff',
                    ),

                    array(
                        'id'      => 'tx_header_link_hover',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Hover Color', 'octa'),
                        'desc'    => esc_html__('Add your menu text hover color.', 'octa'),
                        'default' => '#f55054',
                    ),

                    array(
                        'id'      => 'tx_header_link_active',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Active Color', 'octa'),
                        'desc'    => esc_html__('Add your menu active color.', 'octa'),
                        'default' => '#f55054',
                    ),
                ),
            ),




            array(
                'id'        => 'tx_header_color',
                'type'      => 'fieldset',
                'title'     => esc_html__('Header Color Options', 'octa'),
                'desc'      => esc_html__('Select your header color .', 'octa'),
                'fields'    => array(            
      
                    array(
                        'id'      => 'tx_header_top_bg_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Header Top Background Color', 'octa'),
                        'desc'    => esc_html__('Add your top section bg color.', 'octa'),
                        'default' => '#000',
                    ),

                    array(
                        'id'      => 'tx_header_top_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Header Top Text Color', 'octa'),
                        'desc'    => esc_html__('Add your top section text color.', 'octa'),
                        'default' => '#fff',
                    ),

                    array(
                        'id'      => 'tx_navbar_1_bg_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Header 1 Background Color', 'octa'),
                        'desc'    => esc_html__('Add your header 1 bg color.', 'octa'),
                        'default' => '#fff',
                    ),

                    array(
                        'id'      => 'tx_navbar_2_bg_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Header 2 Background Color', 'octa'),
                        'desc'    => esc_html__('Add your header 2 bg color.', 'octa'),
                        'default' => '#232020',
                    ),

                    array(
                        'id'      => 'page_header_bg_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Page Header Background Color', 'octa'),
                        'desc'    => esc_html__('Add your single page header bg color.', 'octa'),
                        'default' => '#fff',
                    ),

                    array(
                        'id'      => 'page_header_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Page Header Title Color', 'octa'),
                        'desc'    => esc_html__('Add your single page header text color.', 'octa'),
                        'default' => '#5a5a5a',
                    ),

                    array(
                        'id'      => 'post_title_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Post Title Color', 'octa'),
                        'desc'    => esc_html__('Add your post title color.', 'octa'),
                        'default' => '#000',
                    ),
                
                ),
            ),




            /**
             * Footer Link Color
             */

            array(
                'id'        => 'tx_footer_link_color',
                'type'      => 'fieldset',
                'title'     => esc_html__('Footer Color Options', 'octa'),
                'desc'      => esc_html__('Select your footer color.', 'octa'),
                'fields'    => array(

                   array(
                    'id'    => 'tx_footer_title_transform',
                    'type'  => 'select',
                    'title' => esc_html__('Widget Title Transform', 'octa'),
                    'desc'  => esc_html__('Select footer widget transform.', 'octa'),
                    'options'   => array(
                        'uppercase'     => esc_html__('Text Uppercase', 'octa'),
                        'lowercase'     => esc_html__('Text Lowercase', 'octa'),
                        'capitalize'    => esc_html__('Text Capitalize', 'octa'),

                    ),
                    'default'      => 'uppercase'
                    ),

                    array(
                        'id'      => 'footer_widget_title-color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Footer Widgets Title Color', 'octa'),
                        'desc'    => esc_html__('Add your widget title color.', 'octa'),
                        'default' => '#222',
                    ),

                    array(
                        'id'      => 'tx_footer_link_reg',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Regular', 'octa'),
                        'desc'    => esc_html__('Add your footer text color.', 'octa'),
                        'default' => '#fff',
                    ),

                    array(
                        'id'      => 'tx_footer_link_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Link Color', 'octa'),
                        'desc'    => esc_html__('Add your footer link color.', 'octa'),
                        'default' => '#fff',
                    ),
                    array(
                        'id'      => 'tx_footer_link_hover',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Link Hover Color', 'octa'),
                        'desc'    => esc_html__('Add your footer link hover color.', 'octa'),
                        'default' => '#f1868a',
                    ),


                    /**
                     * Footer Backround Color
                     */

                    array(
                        'id'      => 'tx_footer_p_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Footer Background Color', 'octa'),
                        'desc'    => esc_html__('Add your footer bg color.', 'octa'),
                        'default' => '#fafafa',
                    ),

                    /**
                     * Copyright Background Color
                     */

                    array(
                        'id'      => 'tx_copyright_bg_color',
                        'type'    => 'color_picker',
                        'title'   => esc_html__('Copyright Background Color', 'octa'),
                        'desc'    => esc_html__('Add your copyright bg color.', 'octa'),
                        'default' => '#373737',
                    ),

                ),
            ),




        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'style_framework_options' );