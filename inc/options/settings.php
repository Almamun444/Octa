<?php

/**
 * Codestar Framework Settings
 */

function octa_option_settings( $settings ){
    $settings = array();

    $settings      = array(
        'menu_title' => esc_html__('Octa Customizer', 'octa'),
        'menu_type'  => 'menu',
        'menu_slug'  => 'octa-options',
        'menu_icon'  => 'dashicons-editor-table',
        'ajax_save'  => true,
    );

    return $settings;
}

add_filter('cs_framework_settings', 'octa_option_settings');

