<?php

/*

Plugin Name:Octa Recent Post
Plugin URI:https://themesgrove.com
Description:Recent Post Widget
Version:1.1
Author:Themesgrove
Author URI:https://themesgrove.com

*/

class octa_recent_posts extends WP_Widget{
public	function __construct(){
		parent::__construct(false, $name=esc_html__('Octa Recent Post', 'octa'));

	}


function form( $instance ) {
       $defaults = array(
           'title' => '-1'
       );
       $title = $instance[ 'title' ];
       $postno = $instance[ 'postno' ];
       $thumbWidth = $instance[ 'thumbWidth' ];
       $thumbHeight = $instance[ 'thumbHeight' ];

       // markup for form 
       ?>
       <p>
           <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php echo esc_html__('Title', 'octa'); ?></label><br>
           <input class="" type="text" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" value="<?php echo esc_attr( $title ); ?>">
        </p>
     	<p>
			<label for="<?php echo esc_attr( $this->get_field_id('postno') );?>"><?php echo esc_html__('Number The Post :', 'octa'); ?></label>
			<input type="text"
			       id="<?php echo esc_attr( $this->get_field_id('postno') );?>"
			       name="<?php echo esc_attr($this->get_field_name('postno'));?>"
			       value="<?php if(isset($postno)) echo esc_attr($postno);?>"
			       class="">
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('thumbWidth') );?>"><?php echo esc_html__('Thumbnail Width: in px (ex. 100px)', 'octa'); ?> </label>
			<br>
			<input type="text"
			       id="<?php echo esc_attr( $this->get_field_id('thumbWidth') );?>"
			       name="<?php echo esc_attr( $this->get_field_name('thumbWidth') );?>"
			       value="<?php if(isset($thumbWidth)) echo esc_attr($thumbWidth);?>"
			       class="">
		</p>
​
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id('thumbHeight') );?>"><?php echo esc_html__('Thumbnail Height: in px (ex. 100px)', 'octa'); ?> </label>
			<br>
			<input type="text"
			       id="<?php echo esc_attr( $this->get_field_id('thumbHeight') );?>"
			       name="<?php echo esc_attr( $this->get_field_name('thumbHeight') );?>"
			       value="<?php if(isset($thumbHeight)) echo esc_attr($thumbHeight);?>"
			       class="">
		</p>

   

       <?php
   }


public	function update( $new_instance, $old_instance){
	$instance = $old_instance;
	$instance['title'] = strip_tags($new_instance['title']);
	$instance['postno'] = strip_tags( $new_instance['postno'] );
    $instance['thumbWidth'] = $new_instance['thumbWidth'];
    $instance['thumbHeight'] = $new_instance['thumbHeight'];
	return $instance;

}

public	function widget($args, $instance){

extract($args);
extract($instance);

		?>

        <section id="recent-post" class="widget widget-post">
        	<h2 class="widget-title"><?php echo $instance['title']; ?></h2>

        	   <?php 
	        	    $height =   $thumbHeight;
			        $width =  $thumbWidth;
                    $query_args = array(
	                    'order'                 => 'DESC',
						'orderby'               => 'orderby',
						'posts_per_page'        => $instance['postno'],
						'ignore_sticky_posts'    => 1
                    );
                    $query = new Wp_Query($query_args);
                    while($query->have_posts()) : $query->the_post();
        	   ?>

	        	<div class="media">
					<div class="widget-img pull-left">
						<a href="<?php the_permalink(); ?>">
							<?php 
								// check if the post has a Post Thumbnail assigned to it.
								if ( has_post_thumbnail() ) {
									the_post_thumbnail(array($width, $height ));
								}
					        ?>
						</a>
					</div>
					<div class="blog-details media-body">
						<h5 class="post-heading media-heading"> 
						    <a href="<?php the_permalink();?>"> 
							  <?php echo wp_trim_words( get_the_title(), 4, ''); // post title ?>
						    </a>
						</h5>

					    <div class="entry-meta">
							<span><?php the_date('F j, Y'); ?></span>
						</div><!-- .entry-meta -->
					</div>
				</div>
			<?php endwhile;?>
        </section>


	<?php

	}

}

add_action('widgets_init', function(){
	register_widget('octa_recent_posts');
})



?>
