<?php
/**
 * Declaring widgets
 *
 *
 * @package octa
 */
function octa_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Main Sidebar', 'octa' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Main Widget Area.', 'octa' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widget 1', 'octa' ),
		'id'            => 'footer-sidebar-1',
		'description'   => 'Footer Widget Area 1',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
	) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget 2', 'octa' ),
        'id'            => 'footer-sidebar-2',
        'description'   => 'Footer Widget Area 2',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => esc_html__( 'Footer Widget 3', 'octa' ),
        'id'            => 'footer-sidebar-3',
        'description'   => 'Footer Widget Area 3',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

}
add_action( 'widgets_init', 'octa_widgets_init' );