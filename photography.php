<?php
/**
 * Template Name:Photography
 *
 * @package WordPress
 * @subpackage octa
 * @since octa 1.0
 */


	// Sidebar
	$photo_widget_position = cs_get_option('tx_sidebar_position_photo');

	// Banner
	$photo_banner_position = cs_get_option('all_slider_layout');
	

	$banner_position_photo = $photo_banner_position['photography_banner_layout'];

	// slider_groups
	$photo_banner_groups = $photo_banner_position['photo_banner'];

	
	// Banner title transform
	$title_transform = cs_get_option('slider_link_transform');



	get_header(); 
?>

	<?php if($banner_position_photo=='banner') :?>		

	   <!-- Wrapper for personal sliders -->
	    <div class="photo-banner photo-image" 
		    style="
		    	<?php if($photo_banner_groups['photography_bg_layout']=='image') :?>

		   			background-image: url(
		   			 <?php echo wp_get_attachment_image_url($photo_banner_groups['photo_banner_bg_image'], 'full'); ?>);

		   			<?php if($photo_banner_groups['photography_bg_layout']) :?>

		   				background-attachment: <?php echo $photo_banner_groups['tx_banner_parallax']?'fixed': '';    ?>;

		   			<?php endif; ?>

		   		<?php else: ?>
		   			background-color:  <?php echo $photo_banner_groups['photo_banner_bg_color'];?>;
				<?php endif; ?>
		    ">

			<?php 

				if ( !empty($photo_banner_groups) ): ?>
			    <div class="banner-wrapper">

				    <div class="banner-caption">

			            <?php if($photo_banner_groups['photo_banner_title']): ?>
				       		<h1 class="banner-title <?php echo $title_transform; ?>"><?php echo $photo_banner_groups['photo_banner_title'];?></h1>
				        <?php endif; ?>

				        <?php if($photo_banner_groups['photo_banner_desc']): ?>
				       		<p class="banner-content"><?php echo $photo_banner_groups['photo_banner_desc'];?></p>
				        <?php endif; ?>

				        <?php if($photo_banner_groups['photo_banner_btn_text']): ?>
				        	<a class="btn btn-border" target="_blank" href="<?php echo $photo_banner_groups['photo_banner_btn_link'];?>">
				        		<?php echo $photo_banner_groups['photo_banner_btn_text'];?>
				        		
				        	</a>
				        <?php endif; ?>
		            </div>
			    </div>
			    
			<?php endif; ?>
		</div>


		<?php else: ?>
		<div class="video">
			<video autoplay>
				<source src="<?php echo $photo_banner_position['photo_video_banner'];?>" type="video/mp4">
			</video>		
		</div>
	<?php endif; ?>






		<div class="container-fluid">
			<div class="row category-item padding">

				<?php 
					$sticky = array(
						'posts_per_page' =>2 ,
						'category_name'  => 'Photography',
						'post__in'  => get_option( 'sticky_posts' ),
						'ignore_sticky_posts' => 1
					);
					$sticky = new WP_Query( $sticky ); 
					
				while($sticky->have_posts()) : $sticky->the_post();
				?>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="row photo-wrapper">
							<div class="col-md-8 col-sm-12 col-xs-12">								
								<a href="<?php the_permalink(); ?>">
									<div class="sticky-post-thumbnails">
										<?php the_post_thumbnail(); ?>
									</div>
								</a> 
							</div>

							<div class="col-md-4 col-sm-12 col-xs-12">
								<div class="photo-post-details">
									<div class="entry-meta text-uppercase">
										<?php the_date(); ?>
									</div>
									<header class="entry-header">
										<h3>
											<a href="<?php the_permalink(); ?>">
											<?php echo wp_trim_words( get_the_title(),4, ''); ?>
											</a>
										</h3>
									</header>
								</div>
							</div>
						</div>							
					</div>

				<?php endwhile;  wp_reset_query(); ?>
			</div>
		</div>


	<div id="primary" class="content-area padding">

		<main id="main" class="site-main" role="main">


			<div class="container-fluid">

				<div class="row">

		 			<!-- start left sidebar -->

			    	<?php if($photo_widget_position=='left') :?>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					<?php endif; ?>

					<!-- end left sidebar -->

		        	<!-- end of /.col-md-8 or /.col-md-12 -->

					<?php if($photo_widget_position=='no_sidebar') :?>
		                <div class="col-md-12">
		            <?php else: ?>
		                <div class="col-md-8">
		            <?php endif; ?>

			        		<div class="row">
			        		<?php 
			        		$posts_per_page = get_option( 'posts_per_page' );
							$paged = 1;
							if ( get_query_var( 'paged' ) ) $paged = get_query_var( 'paged' );
							if ( get_query_var( 'page' ) ) $paged = get_query_var( 'page' );


			        			$photography_post_args = array (
									'category_name'          => 'Photography',
									'post_type' => 'post',
									'paged' => $paged,
									'posts_per_page' => $posts_per_page
								);

								// The Query
								$photography_query_post = new WP_Query( $photography_post_args); 
							?>

			            		<?php if($photography_query_post->have_posts()): ?>

					                <?php while($photography_query_post->have_posts()): ?>

										<?php $photography_query_post->the_post(); ?>
										<div class="col-md-4">

										<?php get_template_part( 'template-parts/content', 'photography' ); ?>
										</div>
								
									<?php 
										endwhile; 
							 			endif;  
										wp_reset_query(); 
									?>
			          
			        		</div>
			        		<!-- end of /.item -->

			        		<div class="octa-pagination text-center">
								<?php 
									$links = paginate_links( array(
										'prev_text' => '&larr;',
										'next_text' => '&rarr;',
										'total' => $photography_query_post->max_num_pages
									) );

									if ( $links ) {
										echo '<nav class="posts-pagination" role="navigation">';
											echo $links;
										echo '</nav>';
									}

									wp_reset_postdata();
								?>

	        		
        					</div>

			        	</div>
			        	<!-- end of /.col-md-8 or /.col-md-12 -->

		    	    <!-- start right sidebar -->

			    	<?php if($photo_widget_position=='right') :?>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					<?php endif; ?>

				    <!-- end right sidebar -->


				</div>
				<!-- end of /.row -->
			</div>
			<!-- end of /.container -->

		</main><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>

