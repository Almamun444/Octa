<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">

    <input type="search" placeholder="Search" value="<?php echo get_search_query(); ?>" name="s"  />

    <input type="submit" id="searchsubmit"
            value="<?php echo esc_attr_x( 'Search','octa' ); ?>" />
</form>