<?php

/**
 * Getting Option values for Header
 */

$header_variation = cs_get_option('tx_header_select'); //returns text (string)


if( isset($_GET['header']) && $_GET['header'] == 'v1' ) {

    get_template_part( 'template-parts/header/header', 'v1' );

} elseif( isset($_GET['header']) && $_GET['header'] == 'v2' ) {

    get_template_part( 'template-parts/header/header', 'v2' );

} else {

    switch ( $header_variation ) {

        case 'header_1':
            get_template_part( 'template-parts/header/header', 'v1' );
            break;

        case 'header_2':
            get_template_part( 'template-parts/header/header', 'v2' );
            break;

        default:
            get_template_part( 'template-parts/header/header', 'v1' );
    }
}
