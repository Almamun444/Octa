<?php
/**
 * octa functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package octa
 */

if ( ! function_exists( 'octa_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function octa_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on octa, use a find and replace
	 * to change 'octa' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'octa', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'octa' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );


	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'gallery',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'octa_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'octa_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function octa_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'octa_content_width', 640 );
}
add_action( 'after_setup_theme', 'octa_content_width', 0 );




/**
 * Enqueue scripts and styles.
 */
function octa_scripts() {

								// FONTS STYLESHEET
	
	// Cabin font family
	wp_enqueue_style('cabin-fonts',  get_template_directory_uri() . '/dist/fonts/cabin-font.css');

	// Pt-serif font family
	wp_enqueue_style('pt-serif-fonts',  get_template_directory_uri() . '/dist/fonts/pt-serif.css');



								// STYLESHEET
	// Bootstap Stylesheet
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/dist/css/bootstrap.css');

	// Ionsicon Stylesheet
	wp_enqueue_style('ionsicon', get_template_directory_uri() . '/dist/css/ionicons.min.css');

	// Fontawesome Stylesheet
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/dist/css/font-awesome.min.css' );

	// Owlcarousel Stylesheet
	wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/dist/css/owl.carousel.css' );

	// Theme Stylesheet
	wp_enqueue_style( 'octa-style', get_stylesheet_uri() );

	// Custom Stylesheet
	wp_enqueue_style('main-style', get_template_directory_uri() . '/dist/css/style.css', '', '1.0' );


								// JAVASCRIPT 

	// Jequery script
	wp_enqueue_script( 'jquery', get_template_directory_uri() . '/dist/js/jquery.js', array(), '20151215', true );

	// Bootstrap script
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/dist/js/bootstrap.min.js', array(), '20151215', true );

	// Smooth scrool script
	wp_enqueue_script( 'octa-js', get_template_directory_uri() . '/dist/js/smooth-scroll.js', array(), '20151215', true );

	// Masonry script
	wp_enqueue_script( 'masunary-js', get_template_directory_uri() . '/dist/js/masonry.pkgd.min.js', array(), '20151215', true );

	// Owl carousel script
	wp_enqueue_script( 'owl-js', get_template_directory_uri() . '/dist/js/owl.carousel.min.js', array(), '20151215', true );

	// Custom script
	wp_enqueue_script( 'smoothscroll-js', get_template_directory_uri() . '/dist/js/octa.js', array(), '20151215', true );




								// BUILDIN JAVASCRIPT 
	// Navigation script
	wp_enqueue_script( 'octa-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	
	// Skip script
	wp_enqueue_script( 'octa-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'octa_scripts' );




/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Codestar Framework
 */
require_once get_template_directory() . '/libs/cs-framework/cs-framework.php';

/**
 * Load Custom Framework Options
 */
require_once get_template_directory() . '/inc/tx-framework.php';

/**
 * Load Custom Framework Stylesheet
 */
require_once get_template_directory() . '/inc/style.php';

/**
 * Load Custom Framework Stylesheet
 */
require_once get_template_directory() . '/inc/widgets.php';

/**
 * theme recnt post
 */
require get_template_directory() . '/inc/widgets/recent-post.php';

/**
 * theme popular post
 */
require get_template_directory() . '/inc/widgets/popular-post.php';

// Octa functions
require get_template_directory() . '/inc/octa-functions.php';

// Octa functions
require get_template_directory() . '/inc/custom_taxonomy.php';



//to generate shortcode through widget.
add_filter('widget_text', 'do_shortcode');