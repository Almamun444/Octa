
<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package octa
 */
    $query_args = array(
        'order'                 => 'DESC',
		'orderby'               => 'orderby',
    );

	// copyright text
	$copyright_text = cs_get_option('copyright_text');

	// Breadcumb
	$breadcrumb = cs_get_option('tx_breadcrumb');


?>

	<?php if(empty(is_front_page())): ?>
		<?php if(!empty($breadcrumb)): ?>

			<div class="breadcrumb">
		    	<div class="container">
			    	<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">	
							<?php octa_breadcrumb(); ?>
						</div>
					</div>	
				</div>
			</div>
			
		<?php endif; ?>
		<?php endif; ?>

		<div class="container-fluid">
			<div class="row">
				<div class="blog-img">
					<?php  
						$query = new Wp_Query($query_args);
						while($query->have_posts()) : $query->the_post();?>
						<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?>
						<div class="post-title text-center">
							<h5><?php echo wp_trim_words( get_the_title(),4, '' ); // post title ?></h5></div>
						</a> 
				    <?php endwhile;  wp_reset_query(); ?>
				</div>
			</div>
		</div>



		</div><!-- #content -->

			<footer id="colophon" class="site-footer" role="contentinfo">
			    <div class="footer">
					<div class="container">
					    <div class="row">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="footer-sidebar-1">
									<?php if(dynamic_sidebar('footer-sidebar-1')); ?>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="footer-sidebar-2">
									<?php if(dynamic_sidebar('footer-sidebar-2')); ?>
								</div>
							</div>
							<div class="col-md-4 col-sm-6 col-xs-12">
								<div class="footer-sidebar-3">
									<?php if(dynamic_sidebar('footer-sidebar-3')); ?>
								</div>
							</div>
					    </div>
				    </div>
				</div>

				<div class="site-info copyright">
				    <div class="container">
				        <div class="row">
						    <div class="site-info col-md-6">
							    <p><?php echo $copyright_text;?></p>
							</div><!-- .site-info -->

							<div class="nav text-right scroll-top col-md-6">
							  <a class="btn_top" href="#" title="Scroll to top"><i class="ion-ios-arrow-up"></i></a>
							</div>
						</div>
					</div>
				</div><!-- .site-info -->
			</footer><!-- #colophon -->

		<?php wp_footer(); ?>

	</body>
</html>

