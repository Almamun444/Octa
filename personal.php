<?php
/**
 * Template Name:Personal
 *
 * @package WordPress
 * @subpackage octa
 * @since octa 1.0
 */


	// Sidebar
	$personal_widget_position = cs_get_option('tx_sidebar_position_personal');

	// Slider
	$personal_slider_position = cs_get_option('all_slider_layout');
	

	$slider_position_personal = $personal_slider_position['personal_slider_layout'];

	// slider_groups
	$personal_slider_groups = $personal_slider_position['personal_slider'];

	
	// Slider title transform
	$title_transform = cs_get_option('slider_link_transform');



	get_header(); 
?>

	<?php if($slider_position_personal=='image') :?>		

   <!-- Wrapper for personal sliders -->
    <div class="personal-slider">
		<?php 

		if ( !empty($personal_slider_groups) ): ?>

		<?php foreach ( $personal_slider_groups as $slider ):?>
		    <div class="item">
	    	    <?php if($slider['personal_slider_image']): ?>
			        <img src="<?php echo wp_get_attachment_url($slider['personal_slider_image']); ?>" alt="Carousel Slider">
			    <?php endif; ?>

			    <div class="carousel-caption">

		            <?php if($slider['personal_slider_title']): ?>
			       		<h1 class="slider-title <?php echo $title_transform; ?>"><?php echo $slider['personal_slider_title'];?></h1>
			        <?php endif; ?>

			        <?php if($slider['personal_slider_desc']): ?>
			       		<p class="slider-content"><?php echo $slider['personal_slider_desc'];?></p>
			        <?php endif; ?>

			        <?php if($slider['personal_slider_btn_text']): ?>
			        	<a class="btn btn-border" target="_blank" href="<?php echo $slider['personal_slider_btn_link'];?>">
			        		<?php echo $slider['personal_slider_btn_text'];?>
			        		
			        	</a>
			        <?php endif; ?>
	            </div>
		    </div>
		    
		<?php endforeach; endif; ?>
	</div>
<?php endif; ?>



	<div id="primary" class="content-area padding">

		<main id="main" class="site-main" role="main">


			<div class="container">

				<div class="row">

		 			<!-- start left sidebar -->

			    	<?php if($personal_widget_position=='left') :?>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					<?php endif; ?>

					<!-- end left sidebar -->

		        	<!-- end of /.col-md-8 or /.col-md-12 -->

					<?php if($personal_widget_position=='no_sidebar') :?>
		                <div class="col-md-12">
		            <?php else: ?>
		                <div class="col-md-8">
		            <?php endif; ?>

			        		<div class="list">
			        		<?php 
			        		$posts_per_page = get_option( 'posts_per_page' );
							$paged = 1;
							if ( get_query_var( 'paged' ) ) $paged = get_query_var( 'paged' );
							if ( get_query_var( 'page' ) ) $paged = get_query_var( 'page' );


			        			$personal_post_args = array (
									'category_name'          => 'Personal',
									'post_type' => 'post',
									'paged' => $paged,
									'posts_per_page' => $posts_per_page
								);

								// The Query
								$personal_query_post = new WP_Query( $personal_post_args); 
							

			            		if($personal_query_post->have_posts()): 

					                while($personal_query_post->have_posts()): 

										 $personal_query_post->the_post(); 

										 get_template_part( 'template-parts/content', 'personal' ); 
								
									endwhile; 
							 	endif; 



								wp_reset_query();

			                ?>
			          
			        		</div>
			        		<!-- end of /.item -->

			        		<div class="octa-pagination text-center">
								<?php 
								$links = paginate_links( array(
										'prev_text'          => __('« Previous Page', 'octa'),
										'next_text'          => __('Next Page »', 'octa'),
										'total' => $personal_query_post->max_num_pages
									) );

									if ( $links ) {
										echo '<nav class="posts-pagination" role="navigation">';
											echo $links;
										echo '</nav>';
									}

									wp_reset_postdata();
								?>

	        		
        					</div>

			        	</div>
			        	<!-- end of /.col-md-8 or /.col-md-12 -->

		    	    <!-- start right sidebar -->

			    	<?php if($personal_widget_position=='right') :?>
						<div class="col-md-4 col-sm-12 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					<?php endif; ?>

				    <!-- end right sidebar -->


				</div>
				<!-- end of /.row -->
			</div>
			<!-- end of /.container -->

		</main><!-- #main -->

	</div><!-- #primary -->

<?php get_footer(); ?>

