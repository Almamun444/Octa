<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(' personal-post'); ?>>

	<div class="row">

		<div class="col-md-12">
			<div class="personal-wrapper">

				<!-- End post info -->	 
				<div class="full-grid">
					<figure class="hover-effect">
						<div class="personal-thumbnails">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> 
						</div>
						<figcaption class="post-info">
							<?php if ( 'post' === get_post_type() ) : ?>
							<div class="entry-meta">
								<?php if(has_category()):?>
								<!-- Post category -->
									<span><?php the_category(); ?></span>
								<?php endif;?> 

								   <?php if(get_the_date()):?>
									   	<div class="post-date">
											<!-- Post date time  -->
											<span><a href="<?php the_permalink();?>"><?php the_date(); ?></a></span>
									    <?php endif;?>
										<span>
										    <a href="<?php the_permalink();?>">
											  	<?php comments_number( '- no responses', '- 1 Comments', '% responses' ); ?>
											</a>
										</span>
									</div>
								</div><!-- .entry-meta -->
							<?php endif; ?>


							<h3 class="entry-title">
								<a href="<?php the_permalink();?>">
								<?php echo wp_trim_words( get_the_title(),6, ''); ?></a>
							</h3>
							<!-- <a href="#">View more</a> -->
						</figcaption>	

						<div class="entry-content">
							<p><?php echo wp_trim_words( get_the_content(),50, ''); ?></p>	
					    </div>


					    <footer class="entry-footer">		
		  
						<div class="content-btn text-center">
							<a href="<?php the_permalink(); ?>" class="btn btn-border"><?php esc_html_e('Continue Reading', 'octa');?></a>
						</div>
							
					</footer><!-- .entry-footer -->


					</figure>
				</div>

			</div>
		</div>
		<!-- End .spa wrapper --> 

	</div>
	<!-- End .row -->
</article><!-- #post-## -->
