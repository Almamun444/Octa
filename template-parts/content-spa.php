<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(' spa-post main-post'); ?>>

	<div class="row">

		<div class="spa-wrapper">

			<!-- Post thumbnails -->
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="spa-thumbnails post-thumbnails">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> 
				</div>
			</div>
			
			<!-- Post col-md-6 -->
			<div class="col-md-6 col-sm-12 col-xs-12">
				<!-- Post info -->
				<div class="post-info">

						<?php if(has_category()):?>
							<!-- Post category -->
							<span><?php the_category(); ?></span>
						<?php endif;?> 

						<h3 class="entry-title">
							<a href="<?php the_permalink();?>">
							<?php echo wp_trim_words( get_the_title(),7, ''); ?></a>
						</h3>

						<?php if ( 'post' === get_post_type() ) : ?>
							
							<div class="entry-meta">
							   <?php if(get_the_date()):?>
									<!-- Post date time  -->
									<span>
										<a href="<?php the_permalink();?>"><?php the_date(); ?> |</a>
									</span>
							    <?php endif;?>


								<!-- Post author -->
								<span>
									by : 
								    <a href="<?php the_permalink();?>">
									  	<?php the_author(); ?>
									</a>
								</span>
							</div><!-- .entry-meta -->

						<?php endif; ?>

					<div class="entry-content">
						<p><?php echo wp_trim_words( get_the_content(),8, ''); ?></p>	
				    </div>

				</div>
				<!-- End post info -->	 

			</div>
			<!-- End col-md-6 --> 

		</div>
		<!-- End .spa wrapper --> 

	</div>
		<!-- End .row -->
</article><!-- #post-## -->
