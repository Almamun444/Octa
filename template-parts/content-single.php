<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(' main-post'); ?>>
    			
    <div class="single-thumbnails"> 
		<?php the_post_thumbnail(); ?>
	</div>
	
	<div class="single-post-details">
	
		<header class="entry-header">
			<?php if(has_category()):?>
				<!-- Post category -->
				<span><?php the_category(); ?></span>
			<?php endif;?> 


			<?php
				the_title( '<h1 class="entry-title">', '</h1>' );

			if ( 'post' === get_post_type() ) : ?>
				
				<div class="entry-meta">
				   <?php if(get_the_date()):?>
						<!-- Post date time  -->
						<span><a href="<?php the_permalink();?>"><?php the_date(); ?>  / </a></span>
				    <?php endif;?>

				    <?php if(get_the_author()):?>
				    	<span><a href="<?php the_permalink();?>"><?php the_author(); ?></a></span>
				    <?php endif;?>

						<span>
						    <a href="<?php the_permalink();?>">
							  	<?php comments_number( '/ no responses', '- 1 Comments', '% responses' ); ?>
							</a>
						</span>
				</div><!-- .entry-meta -->
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content">
			<p><?php the_content();?></p>
	    </div>

		<footer class="entry-footer">	
		    <div class="post-meta pull-left"> 
				<?php if(has_tag()):?>
					<!-- Post tag -->
					<span><?php the_tags('', ' ', '<br />'); ?></span>
			    <?php endif;?>
			</div>
		    <div class="social-share pull-right">
				<?php echo octa_social_share(); ?>
			</div>
		</footer><!-- .entry-footer -->
	</div>
</article><!-- #post-## -->
