<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package octa
 */

	// preloader swith
	$preload_switch = cs_get_option('tx-enable-preloader'); 

	// Custom Logo
    $logo_1 = cs_get_option('tx_logo_1');

    // Top section switch
	$soical_switch = cs_get_option('tx_social_icons'); 
	
	// top detials id
	$top_contact = cs_get_option('tx_top_text');
	// Top location text
	$top_location = $top_contact['tx_top_location'];
	// Top call id
	$top_call = $top_contact['tx_top_call'];
	// Top mail id
	$top_mail = $top_contact['tx_top_mail'];

	// Top social id
	$social_links = cs_get_option('tx_top_social');
	// Social icon facebook
	$social_fb = $social_links['tx_top_social_fb'];
	// Social icon twitter
	$social_tw = $social_links['tx_top_social_tw'];
	// Social icon goole
	$social_gm = $social_links['tx_top_social_gm'];
	// Social icon pinterest
	$social_pi = $social_links['tx_top_social_pi'];
	// Social icon vimeo
	$social_vm = $social_links['tx_top_social_vm'];

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text " href="#content"><?php esc_html_e( 'Skip to content', 'octa' ); ?></a>

	<header id="masthead" class="site-header head-1" role="banner">

		<?php if(!empty($soical_switch)): ?>
			<div class="top-section">
				<div class="container">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<ul class="list-inline">
								<?php if($top_location): ?>
									<li><i class="ion-ios-location"></i><?php echo $top_location; ?></li>
								<?php endif; ?>

								<?php if($top_call): ?>
									<li><i class="ion-ios-telephone"></i><?php echo $top_call; ?></li>
								<?php endif; ?>

								<?php if($top_mail): ?>
									<li><i class="ion-email"></i><?php echo $top_mail; ?></li>
								<?php endif; ?>
							</ul>
						</div>


					    <div class="col-md-6 col-sm-6 col-xs-12">
						    <div class="topMenu-social text-right">

								<?php if(!empty($social_fb)): ?>
									<a href="<?php echo esc_url($social_fb); ?>" target="_blank" class="tm-facebook">
										<i class="ion-social-facebook fa-fw"></i>
									</a>
								<?php endif; ?>
								
								<?php if(!empty($social_tw)): ?>
									<a href="<?php echo esc_url($social_tw); ?>" target="_blank" class="tm-twitter">
										<i class="ion-social-twitter fa-fw"></i>
									</a>
								<?php endif; ?>

								<?php if(!empty($social_gm)): ?>
									<a href="<?php echo esc_url($social_gm); ?>" target="_blank" class="tm-googleplus">
										<i class="ion-social-googleplus fa-fw"></i>
									</a>
								<?php endif; ?>

								<?php if(!empty($social_vm)): ?>
									<a href="<?php echo esc_url($social_vm); ?>" target="_blank" class="tm-vimeo">
										<i class="ion-social-linkedin fa-fw"></i>
									</a>
								<?php endif; ?>
								<?php if(!empty($social_pi)): ?>
									<a href="<?php echo esc_url($social_pi); ?>" target="_blank" class="tm-pinterest">
										<i class="ion-social-pinterest fa-fw"></i>
									</a>
								<?php endif; ?>

							</div>
						</div>
					</div>
				</div>
		    </div>
		<?php endif; ?>

		<?php if(!empty($preload_switch)): ?>
			<div class="preloader"></div>
		<?php endif; ?>

		<nav class="navbar navbar-default text-center">
			<div class="container">
		    	<div class="header-search-form">
			        <?php get_search_form();?>
		        </div>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only"><?php esc_html_e('Toggle navigation', 'octa');?></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
					<?php if (isset($logo_1)) :?>
					
						<h1 class="site-title">
							<a  class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>"><img class="site-logo" src="<?php echo wp_get_attachment_url($logo_1); ?>" alt="<?php bloginfo('name'); ?>" /></a>
						</h1>
					<?php else : ?>

						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<p class="site-description"><?php bloginfo( 'description' ); ?></p>
					<?php endif; ?>
				</div>

				 <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav navbar-nav') ); ?>
					<div class="search-icon">
						<span><i class="s-toggle ion-search"></i></span>
					</div>
				</div>
			</div>
		</nav>

	</header><!-- #masthead -->


	<div id="content" class="site-content">