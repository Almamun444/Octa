<?php
/**
 * Template part for displaying results in search pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 

				if ( 'post' === get_post_type() ) : ?>
				
				<div class="entry-meta">
				   <?php if(get_the_date()):?>
						<!-- Post date time  -->
						<span>
							<a href="<?php the_permalink();?>"><?php the_date(); ?> / </a>
						</span>
				    <?php endif;?>

				    <?php if(get_the_author()):?>
				    	<span> <a href="<?php the_permalink();?>"><?php the_author(); ?></a></span>
				    <?php endif;?>

						<span>
						    <a href="<?php the_permalink();?>">
							  	<?php comments_number( '/ no responses', '- 1 Comments', '% responses' ); ?>
							</a>
						</span>
				</div><!-- .entry-meta -->
			<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php the_content(); ?>
	</div><!-- .entry-summary -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'octa' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
