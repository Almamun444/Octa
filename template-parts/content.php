<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(' main-post fashion-post'); ?>>

    	<div class="post-thumbnails"> 
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> 	
		</div>

	<div class="entry-meta text-center">
		<?php if(has_category()):?>
			<!-- Post category -->
			<span><?php the_category(); ?></span>
		<?php endif;?>
	</div> 
	<header class="entry-header text-center">

		<h3 class="text-center entry-title">
			<a href="<?php the_permalink();?>">
			<?php echo wp_trim_words( get_the_title(),4, ''); ?></a>
		</h3>

		<?php if ( 'post' === get_post_type() ) : ?>
			
			<div class="entry-meta">
			   <?php if(get_the_date()):?>
					<!-- Post date time  -->
					<span><a href="<?php the_permalink();?>"><?php the_date(); ?></a></span>
			    <?php endif;?>
				<span>
				    <a href="<?php the_permalink();?>">
					  	<?php comments_number( '- no responses', '- 1 Comments', '% responses' ); ?>
					</a>
				</span>
			</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<p class="text-center"><?php echo wp_trim_words( get_the_content(),30, ''); ?></p>	
    </div>

	<footer class="entry-footer">		
		  
		<div class="content-btn text-center">
			<a href="<?php the_permalink(); ?>" class="btn btn-border"><?php esc_html_e('Continue Reading', 'octa');?></a>
		</div>
			
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
