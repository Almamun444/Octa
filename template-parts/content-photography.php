<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */

?>

		<article id="post-<?php the_ID(); ?>" <?php post_class(' photo-post'); ?>>

			<div class="photo-post-wrapper">
				<div class="photo-thumbnails">
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a> 
				</div>

				<div class="post-info">
					<?php if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta"> 

						   <?php if(get_the_date()):?>
							   	<div class="post-date">
									<!-- Post date time  -->
									<span><a href="<?php the_permalink();?>"><?php the_date(); ?></a></span>
							    <?php endif;?>
								<span>
								    <a href="<?php the_permalink();?>">
									  	<?php comments_number( '- no responses', '- 1 Comments', '% responses' ); ?>
									</a>
								</span>
							</div>
						</div><!-- .entry-meta -->

					<?php endif; ?>

					<figcaption>
						<h3 class="entry-title pull-left">
							<a href="<?php the_permalink();?>">
							<?php echo wp_trim_words( get_the_title(),4, ''); ?></a>
						</h3>
						<div class="social-share pull-right">
							<?php echo octa_social_share(); ?>
						</div>
					</figcaption>
				</div>	
			</div>
			<!-- End .spa wrapper --> 

		<!-- End .row -->
		</article><!-- #post-## -->
