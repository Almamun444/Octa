<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package octa
 */

	// Sidebar
	$widget_position = cs_get_option('tx_sidebar_position');

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<div class="row">

			    	<?php if($widget_position=='left') :?>
			 			<!-- start left sidebar -->
						<div class="col-md-4 col-sm-4 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
						<!-- end left sidebar -->
					<?php endif; ?>

					<?php if($widget_position=='no_sidebar') :?>
	                    <div class="col-md-12 search-page">
	                <?php else: ?>
	                    <div class="col-md-8 search-page">
	                <?php endif; ?>

						<?php
						if ( have_posts() ) : ?>

							<header class="page-header">
								<h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'octa' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
							</header><!-- .page-header -->

							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();

								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'template-parts/content', 'search' );

							endwhile;

							the_posts_navigation();

						else :

							get_template_part( 'template-parts/content', 'none' );

						endif; ?>
			        </div>

			        <?php if($widget_position=='right') :?>
		    	    <!-- start right sidebar -->
					<div class="col-md-4 col-sm-4 col-xs-12">
						<?php get_sidebar(); ?> 
					</div>
				    <!-- end right sidebar -->
				<?php endif; ?>
			    </div>
			</div>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
