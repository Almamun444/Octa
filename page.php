<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package octa
 */
	
	// Sidebar position
	$widget_position = cs_get_option('tx_sidebar_position');

	get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main page padding" role="main">
			<div class="container">
				<div class="row">

			        <div class="col-md-12">

							<header class="page-header text-center">
								<?php
									the_title( '<h1 class="entry-title">', '</h1>' );
								?>
							</header><!-- .page-header -->
					</div>

		    	<?php if($widget_position=='left') :?>
			 	<!-- start left sidebar -->
					<div class="col-md-4 col-sm-12 col-xs-12">
						<?php get_sidebar(); ?> 
					</div>
				<!-- end left sidebar -->
				<?php endif; ?>

				<?php if($widget_position=='no_sidebar') :?>
                    <div class="col-md-12">
                <?php else: ?>
                    <div class="col-md-8">
                <?php endif; ?>


					<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'page' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>
					</div>
			    	<?php if($widget_position=='right') :?>
						<div class="col-md-4 col-sm-12 col-xs-12">
					    <!-- start right sidebar -->
							<?php get_sidebar(); ?> 
						<!-- end right sidebar -->
						</div>
					<?php endif; ?>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
