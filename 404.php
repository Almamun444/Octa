<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package octa
 */


	$widget_position = cs_get_option('tx_sidebar_position');

	$error_image = cs_get_option('404_image');

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container">
				<div class="row">

			    	<?php if($widget_position=='left') :?>
				 	    <!-- start left sidebar -->
						<div class="col-md-4 col-sm-4 col-xs-12">
							<?php get_sidebar(); ?> 
						</div>
					    <!-- end left sidebar -->
					<?php endif; ?>

					<?php if($widget_position=='no_sidebar') :?>
	                    <div class="col-md-12">
	                <?php else: ?>
	                    <div class="col-md-8">
	                <?php endif; ?>

						<section class="error-404 not-found text-center">
							<header class="page-header">
								<?php if (isset($error_image)) :?>
								<img src="<?php echo wp_get_attachment_url($error_image); ?>" alt="404 image">
							<?php else : ?>
								<h1 class="page-title"><?php esc_html_e( '404! That content can&rsquo;t be found.', 'octa' ); ?></h1>
							<?php endif; ?>
							</header><!-- .page-header -->

							<div class="page-content">
								<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'octa' ); ?></p>

								<?php
									get_search_form();

								?>
								<a class="btn btn-404" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Backo To Home', 'octa')?></a>

							</div><!-- .page-content -->
						</section><!-- .error-404 -->
					</div>

					<?php if($widget_position=='right') :?>
						<div class="col-md-4 col-sm-4 col-xs-12">
					    <!-- start right sidebar -->
							<?php get_sidebar(); ?> 
						<!-- end right sidebar -->
						</div>
					<?php endif; ?>
				</div>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
