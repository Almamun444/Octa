var gulp = require('gulp');
var less = require('gulp-less');
var mainBowerFiles = require('main-bower-files');
var gulpFilter = require('gulp-filter');
var minify = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');
var clean = require('gulp-clean');
var zip = require("gulp-zip");
var wpPot = require('gulp-wp-pot');
var sort = require('gulp-sort');

var dest = './dist';
var src = './assets';

var config = {
    url: "http://octa.dev/",
    less: {
        src: [src + '/less/*.less', src + '/css/*.css', src + '/less/styles/*.less'],
        dest: dest + '/css',
    },
    js: {
        src: src + '/js/*.js',
        dest: dest + '/js',
    },
    images: {
        src: src + '/images/**/*',
        dest: dest + '/images'
    },
    fonts: {
        src: src + '/fonts/**/*',
        dest: dest + '/fonts'
    },
    bower: {
        js: dest + '/js',
        css: dest + '/css',
        images: dest + '/images',
        fonts: dest + '/fonts'
    },
    watch: {
        less: src + '/less/**/*',
        js: src + '/js/*.js',
        images: src + '/images/**/*',
        fonts: src + '/fonts/**/*',
        tasks: ['build']
    }
};


gulp.task('less', function () {
    console.log("compiling less");
    return gulp.src(config.less.src)
        .pipe(less(config.less.settings))
        .pipe(gulp.dest(config.less.dest));
});

gulp.task('js', function () {
    return gulp.src(config.js.src)
        .pipe(uglify())
        .pipe(gulp.dest(config.js.dest));
});

gulp.task('fonts', function () {
    return gulp.src(config.fonts.src)
        .pipe(gulp.dest(config.fonts.dest));
});

gulp.task('images', function () {
    return gulp.src(config.images.src)
        .pipe(gulp.dest(config.images.dest));
});

/** move main bower files inside dest directory **/
gulp.task('bower', function () {
    /*** Filters for bower main files **/
    var jsFilter = gulpFilter('**/*.js', {restore: true}),
        cssFilter = gulpFilter('**/*.css', {restore: true}),
        lessFilter = gulpFilter('**/*.less', {restore: true}),
        fontFilter = gulpFilter(['**/*.svg', '**/*.eot', '**/*.woff', '**/*.woff2', '**/*.ttf'], {restore: true}),
        imgFilter = gulpFilter(['**/*.png', '**/*.gif', '**/*.jpg'], {restore: true});

    return gulp.src(mainBowerFiles())

        .pipe(jsFilter)
        .pipe(uglify())
        .pipe(gulp.dest(config.bower.js))
        .pipe(jsFilter.restore)

        .pipe(lessFilter)
        .pipe(less())
        .pipe(gulp.dest(config.bower.css))
        .pipe(rename({extname: 'css'}))
        .pipe(lessFilter.restore)

        .pipe(cssFilter)
        .pipe(minify())
        .pipe(gulp.dest(config.bower.css))
        .pipe(cssFilter.restore)

        .pipe(imgFilter)
        .pipe(gulp.dest(config.bower.images))
        .pipe(imgFilter.restore)

        .pipe(fontFilter)
        .pipe(gulp.dest(config.bower.fonts))
        .pipe(fontFilter.restore);
});

gulp.task('sync', function(){
    //shell.exec(`browser-sync start --proxy "${config.url}" --files "**/*, !assets, !node_modules, !bower_components"`);
});

/** watch on every less js fonts and images change **/
gulp.task('watch', function () {
    console.log("watching..");
    gulp.watch(config.watch.less, ['less']);
    gulp.watch(config.watch.js, ['js']);
    gulp.watch(config.watch.image, ['image']);
    gulp.watch(config.watch.fonts, ['fonts']);
});

/** removes the destination directory */
gulp.task('build-clean', function () {
    return gulp.src(dest).pipe(clean());
});

/** runs build specific tasks after cleaning is done **/
gulp.task('build', function (cb) {
    return runSequence('build-clean', ['js', 'fonts', 'bower', 'images', 'less'], cb);
});

/** after  build is done run watch to recompile on every change**/
gulp.task('default', function () {
    runSequence('build', ['watch']);
});


gulp.task('package', function() {
    gulp.src([
            './*',
            './*/**',
            '!./bower.json',
            '!./package.json',
            '!./gulpfile.js',
            '!./bower_components/**',
            '!./node_modules/**',
            '!./assets/**',
            '!./bower_components',
            '!./node_modules',
            '!./assets'

        ])
        .pipe(zip('octa.zip'))
        .pipe(gulp.dest('.'));
});

gulp.task('generatePot', function () {
    return gulp.src([
            './*.php',
            './*/**.php',
            '!./bower_components/**',
            '!./node_modules/**',
            '!./assets/**',
            '!./bower_components',
            '!./node_modules',
            '!./assets' ])
        .pipe(sort())
        .pipe(wpPot( {
            domain: 'octa',
            destFile:'octa.pot',
            package: 'octa',
            bugReport: 'https://www.themexpert.com',
            lastTranslator: 'ThemeXpert <info@themexpert.com>',
            team: 'ThemeXpert <info@themexpert.com>'
        } ))
        .pipe(gulp.dest('languages'));
});